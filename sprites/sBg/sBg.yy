{
    "id": "d6b78ab9-f030-4041-8106-32c1a2c924ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1919,
    "bbox_left": 0,
    "bbox_right": 1079,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0028c04a-66b6-4c5a-a943-3ab616b33b14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b78ab9-f030-4041-8106-32c1a2c924ac",
            "compositeImage": {
                "id": "b776fb9f-abc4-4ba0-a939-8bc536de598f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0028c04a-66b6-4c5a-a943-3ab616b33b14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68cec644-b758-46a5-9571-d695f1c75677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0028c04a-66b6-4c5a-a943-3ab616b33b14",
                    "LayerId": "39dd7867-21b7-4382-ad9b-74af19c4c656"
                }
            ]
        },
        {
            "id": "5e7a19b6-9f1a-4198-8f0d-6d5ed9f8cacd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b78ab9-f030-4041-8106-32c1a2c924ac",
            "compositeImage": {
                "id": "2b1dbfd6-bb0b-4a13-bea2-9f3439540ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7a19b6-9f1a-4198-8f0d-6d5ed9f8cacd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ad5325-962b-48ae-9f50-60743a7e3323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7a19b6-9f1a-4198-8f0d-6d5ed9f8cacd",
                    "LayerId": "39dd7867-21b7-4382-ad9b-74af19c4c656"
                }
            ]
        },
        {
            "id": "ac281a40-9c29-460d-89be-7bf18029d94b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b78ab9-f030-4041-8106-32c1a2c924ac",
            "compositeImage": {
                "id": "06491b13-2da8-43d6-86c5-b1abbb520902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac281a40-9c29-460d-89be-7bf18029d94b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e97e68cc-a0ef-46c8-aca4-ccec5080d2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac281a40-9c29-460d-89be-7bf18029d94b",
                    "LayerId": "39dd7867-21b7-4382-ad9b-74af19c4c656"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1920,
    "layers": [
        {
            "id": "39dd7867-21b7-4382-ad9b-74af19c4c656",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6b78ab9-f030-4041-8106-32c1a2c924ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1080,
    "xorig": 1552,
    "yorig": 609
}