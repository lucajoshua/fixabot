{
    "id": "53f984dc-ac5f-48c6-bf12-13451a7870c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTimer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 1,
    "bbox_right": 189,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f1deaee-e61d-4099-b2b2-2744bf3a5148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f984dc-ac5f-48c6-bf12-13451a7870c4",
            "compositeImage": {
                "id": "a30fb1b1-5fcd-40d4-af51-854f6bb05f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1deaee-e61d-4099-b2b2-2744bf3a5148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96a489c3-0e21-41cc-b6e7-c9181e961c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1deaee-e61d-4099-b2b2-2744bf3a5148",
                    "LayerId": "aadd2879-245c-450a-91c2-0a655cab8711"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "aadd2879-245c-450a-91c2-0a655cab8711",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53f984dc-ac5f-48c6-bf12-13451a7870c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 95,
    "yorig": 75
}