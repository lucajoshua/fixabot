{
    "id": "ba0a9551-0e69-487d-8364-c406443c25e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 226,
    "bbox_left": 5,
    "bbox_right": 324,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbb62e30-9087-4330-86e1-429b9e885905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba0a9551-0e69-487d-8364-c406443c25e3",
            "compositeImage": {
                "id": "4746bea2-0176-4b16-9021-b2c871226ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb62e30-9087-4330-86e1-429b9e885905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8594a0f5-6ad8-4754-b2e7-2b436f3e27cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb62e30-9087-4330-86e1-429b9e885905",
                    "LayerId": "b0995eb3-c3ad-47c2-988f-1d1589be6ef4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "b0995eb3-c3ad-47c2-988f-1d1589be6ef4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0a9551-0e69-487d-8364-c406443c25e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 120
}