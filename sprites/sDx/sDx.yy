{
    "id": "8b091837-d22b-42a6-96ec-0acf76abf80e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 1,
    "bbox_right": 241,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f0de1f3-8b95-416e-a6ca-a78fe978d686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "98c2c6a3-e7d9-47c6-b9b3-bc159e38419f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0de1f3-8b95-416e-a6ca-a78fe978d686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "543d5239-03ff-48b8-8526-97096d6925c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0de1f3-8b95-416e-a6ca-a78fe978d686",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        },
        {
            "id": "fe4f0d94-fff5-40ba-8d2d-9cef09193998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "c99ad62b-b84f-48d2-a1ef-8a084d00664e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4f0d94-fff5-40ba-8d2d-9cef09193998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aacbd144-060c-4fcc-96ce-9b0d4948abb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4f0d94-fff5-40ba-8d2d-9cef09193998",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        },
        {
            "id": "de34763f-688e-4a7c-8d95-f4a7053ffb65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "22f34835-0ef4-4cbe-b643-248e2ed9047a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de34763f-688e-4a7c-8d95-f4a7053ffb65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d34ac20-18b2-4a37-a202-fa3f9977a998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de34763f-688e-4a7c-8d95-f4a7053ffb65",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        },
        {
            "id": "6a889301-7ea2-4b28-b796-f90ecd998315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "72852646-05c5-4156-ab78-c59c245250b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a889301-7ea2-4b28-b796-f90ecd998315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27822b75-c299-43fe-ba7a-1c5deed32859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a889301-7ea2-4b28-b796-f90ecd998315",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        },
        {
            "id": "7d826f48-de82-47a1-9477-f1c621e09e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "415c64ca-6c1a-47c4-bd4c-9240a8bd424f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d826f48-de82-47a1-9477-f1c621e09e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c37673-3b49-4aa4-833d-3adff5fc90fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d826f48-de82-47a1-9477-f1c621e09e56",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        },
        {
            "id": "64b4deaf-f65a-4912-a3a0-97683439c77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "compositeImage": {
                "id": "c0a61e16-8897-4934-9522-de21b165c2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b4deaf-f65a-4912-a3a0-97683439c77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26cefb78-70d4-4bc1-9e94-b07fb9bf0521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b4deaf-f65a-4912-a3a0-97683439c77d",
                    "LayerId": "0ead7ff4-44e5-427b-bc8a-b89032626238"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 541,
    "layers": [
        {
            "id": "0ead7ff4-44e5-427b-bc8a-b89032626238",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b091837-d22b-42a6-96ec-0acf76abf80e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 0,
    "yorig": 0
}