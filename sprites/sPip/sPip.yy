{
    "id": "1a53a72e-7428-4a05-aa4a-7b2c306d1d60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 397,
    "bbox_left": 2,
    "bbox_right": 247,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69388951-14e7-4fac-a9dd-edcaa8b291fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a53a72e-7428-4a05-aa4a-7b2c306d1d60",
            "compositeImage": {
                "id": "af7bb2de-a77f-408b-8e54-f5ccf663c238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69388951-14e7-4fac-a9dd-edcaa8b291fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040a0e26-39a3-48ba-8c7c-191fe835c18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69388951-14e7-4fac-a9dd-edcaa8b291fd",
                    "LayerId": "39cfe152-537d-43cc-bc80-6edd5bbf9e3e"
                }
            ]
        },
        {
            "id": "a0f06647-18ca-49aa-92f9-51d4a11077ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a53a72e-7428-4a05-aa4a-7b2c306d1d60",
            "compositeImage": {
                "id": "22e34d84-4d31-4e6b-8146-a8c4baea7053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f06647-18ca-49aa-92f9-51d4a11077ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b9ef33-7e12-4f12-9ab2-b25e58e007d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f06647-18ca-49aa-92f9-51d4a11077ff",
                    "LayerId": "39cfe152-537d-43cc-bc80-6edd5bbf9e3e"
                }
            ]
        },
        {
            "id": "e9e5df8a-9952-4722-9b15-46a78eadeee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a53a72e-7428-4a05-aa4a-7b2c306d1d60",
            "compositeImage": {
                "id": "c97cf930-e032-4ee4-a3c1-66f804235860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e5df8a-9952-4722-9b15-46a78eadeee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0370f428-e1d0-4328-9ae8-bc77b5ade338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e5df8a-9952-4722-9b15-46a78eadeee6",
                    "LayerId": "39cfe152-537d-43cc-bc80-6edd5bbf9e3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "39cfe152-537d-43cc-bc80-6edd5bbf9e3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a53a72e-7428-4a05-aa4a-7b2c306d1d60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 248,
    "xorig": 0,
    "yorig": 0
}