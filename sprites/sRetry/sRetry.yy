{
    "id": "7d619d9e-f210-4213-b8fb-d845d6cf1d88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRetry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 232,
    "bbox_left": 15,
    "bbox_right": 334,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56e6b3e4-b14f-4b7b-b1a6-4df30c06a9a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d619d9e-f210-4213-b8fb-d845d6cf1d88",
            "compositeImage": {
                "id": "4d473c3d-8ba5-4f22-8a92-8530d3d57c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e6b3e4-b14f-4b7b-b1a6-4df30c06a9a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96e6c39-9b89-4e7e-8c37-4af8d96f30cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e6b3e4-b14f-4b7b-b1a6-4df30c06a9a1",
                    "LayerId": "c33033f4-6ca8-4b9f-8163-748619b6d0db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "c33033f4-6ca8-4b9f-8163-748619b6d0db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d619d9e-f210-4213-b8fb-d845d6cf1d88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 349,
    "xorig": 0,
    "yorig": 0
}