{
    "id": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLegs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 540,
    "bbox_left": 4,
    "bbox_right": 437,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08002eca-d483-4ae9-94c2-987fee35e7bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "e92652af-1646-481b-85f6-34033d2a093c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08002eca-d483-4ae9-94c2-987fee35e7bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0386b9-109f-4b9a-bb8a-f17bd2e708e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08002eca-d483-4ae9-94c2-987fee35e7bd",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        },
        {
            "id": "9303c5aa-a7dd-4355-9425-927f65ca55ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "681727cf-0b34-4ad6-b81b-d6c925f1dda4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9303c5aa-a7dd-4355-9425-927f65ca55ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d308746-4db6-4461-ae2f-041aaab31690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9303c5aa-a7dd-4355-9425-927f65ca55ed",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        },
        {
            "id": "258190cd-c5f5-4544-b56d-b086f3332bd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "eb5c5b9b-fe4c-46d0-bc1b-de5d8e604160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "258190cd-c5f5-4544-b56d-b086f3332bd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47094bc-0e8a-4b8b-b8fc-d294492ee377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "258190cd-c5f5-4544-b56d-b086f3332bd3",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        },
        {
            "id": "298212c4-f002-4efe-ad99-372df8cd98a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "d076b948-4e1b-4b04-a7fa-031e04b931b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298212c4-f002-4efe-ad99-372df8cd98a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f68b28f-1e85-4b4f-8579-f1196b81159d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298212c4-f002-4efe-ad99-372df8cd98a7",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        },
        {
            "id": "84a86094-3f7d-4bab-84a6-f842c88fa3a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "1e074420-c62e-48e0-bb00-7e5561c79902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a86094-3f7d-4bab-84a6-f842c88fa3a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ddd44b0-7a84-4593-ade6-da1912edc2a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a86094-3f7d-4bab-84a6-f842c88fa3a4",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        },
        {
            "id": "b32b4a78-0443-4d1e-b830-527e97d78413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "compositeImage": {
                "id": "c248f333-5b59-4d1d-b81c-cf98908bac3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32b4a78-0443-4d1e-b830-527e97d78413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4e52c3-bf57-4ce2-a054-6edd4ab824ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32b4a78-0443-4d1e-b830-527e97d78413",
                    "LayerId": "e3897995-9c64-4c88-846b-ed4adc9fa94f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 541,
    "layers": [
        {
            "id": "e3897995-9c64-4c88-846b-ed4adc9fa94f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 441,
    "xorig": 220,
    "yorig": 540
}