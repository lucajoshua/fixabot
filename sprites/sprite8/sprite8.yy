{
    "id": "f966b7ba-da71-4157-a771-d298750967f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 523,
    "bbox_left": 30,
    "bbox_right": 241,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6109f6a5-0ed4-43c2-b15f-2ca23a6f0e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f966b7ba-da71-4157-a771-d298750967f0",
            "compositeImage": {
                "id": "27642096-6b3c-4c44-bd92-3ab1b91a1633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6109f6a5-0ed4-43c2-b15f-2ca23a6f0e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20b1af5-8818-4482-ba7f-5dd29cdb66a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6109f6a5-0ed4-43c2-b15f-2ca23a6f0e45",
                    "LayerId": "20a99660-4892-48be-90ca-73f062f13ed2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 541,
    "layers": [
        {
            "id": "20a99660-4892-48be-90ca-73f062f13ed2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f966b7ba-da71-4157-a771-d298750967f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 122,
    "yorig": 540
}