{
    "id": "11ffae03-68a2-4f88-8ffd-3e8ec79feeb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIntro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1420,
    "bbox_left": 9,
    "bbox_right": 976,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e492b8cc-3a2c-4af7-95c4-b1aea89446c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ffae03-68a2-4f88-8ffd-3e8ec79feeb1",
            "compositeImage": {
                "id": "dca3290f-a69c-4c98-bff3-29f782f09509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e492b8cc-3a2c-4af7-95c4-b1aea89446c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a873f7-4aab-4dfa-bbdd-63f41c08be88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e492b8cc-3a2c-4af7-95c4-b1aea89446c8",
                    "LayerId": "47404e54-8cd2-4667-9b5b-f0078df1a136"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1424,
    "layers": [
        {
            "id": "47404e54-8cd2-4667-9b5b-f0078df1a136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11ffae03-68a2-4f88-8ffd-3e8ec79feeb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 983,
    "xorig": 491,
    "yorig": 712
}