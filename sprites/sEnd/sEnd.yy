{
    "id": "bb5604c5-a2a2-4204-ae23-05da632443be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1152,
    "bbox_left": 0,
    "bbox_right": 1079,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b46b64a-bee2-4927-bf5e-ba5ad9120d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5604c5-a2a2-4204-ae23-05da632443be",
            "compositeImage": {
                "id": "56f656a4-ecab-43a1-b409-82c147799bca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b46b64a-bee2-4927-bf5e-ba5ad9120d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a490bba2-128d-4f32-9776-11d706b0caf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b46b64a-bee2-4927-bf5e-ba5ad9120d76",
                    "LayerId": "afede8c7-3482-47a8-8ec0-e000166fb430"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1153,
    "layers": [
        {
            "id": "afede8c7-3482-47a8-8ec0-e000166fb430",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb5604c5-a2a2-4204-ae23-05da632443be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1080,
    "xorig": 0,
    "yorig": 0
}