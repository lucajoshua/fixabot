{
    "id": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 540,
    "bbox_left": 1,
    "bbox_right": 440,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc75bf53-5dab-4d57-9874-edf220fa624e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "6d85f890-3e5b-4a36-a289-b9af2767634c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc75bf53-5dab-4d57-9874-edf220fa624e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84fbb0f7-57f3-4ad9-a727-261c63de4c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc75bf53-5dab-4d57-9874-edf220fa624e",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        },
        {
            "id": "035127a2-f382-4ffe-8ae8-11ece3a15db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "0fa0aeb6-3a6e-4c67-a7f8-30b7ca4e48b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035127a2-f382-4ffe-8ae8-11ece3a15db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf50995-679f-4f16-bb3b-5f566dd57c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035127a2-f382-4ffe-8ae8-11ece3a15db1",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        },
        {
            "id": "8ad6573a-c150-43a6-be3a-d87d066596e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "9c7ec144-3507-467a-8f33-4ae784d32054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ad6573a-c150-43a6-be3a-d87d066596e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7af327d7-d145-41fb-adc3-175ff6762da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ad6573a-c150-43a6-be3a-d87d066596e3",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        },
        {
            "id": "c760742a-8d9a-4832-acc3-31cdb293448e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "e7bc0348-6541-41c8-b7c7-1e8f2f8a7ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c760742a-8d9a-4832-acc3-31cdb293448e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2b3cd4-8ca7-4a77-9690-59fafe768df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c760742a-8d9a-4832-acc3-31cdb293448e",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        },
        {
            "id": "806931f0-f7bf-43a7-a258-0305d27b3c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "b162d0d0-46a2-4acd-8edc-4b2b05081c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "806931f0-f7bf-43a7-a258-0305d27b3c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d6516d-5f7d-4b06-9484-887b93dfa144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "806931f0-f7bf-43a7-a258-0305d27b3c17",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        },
        {
            "id": "09baba6a-766a-4027-a564-cb3013cd09f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "compositeImage": {
                "id": "1ed92f65-1f1b-44ed-8a2e-e09293bdb436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09baba6a-766a-4027-a564-cb3013cd09f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4629e3-1169-49c0-8148-d42d41f91ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09baba6a-766a-4027-a564-cb3013cd09f5",
                    "LayerId": "b67c5fef-c158-4e22-a967-221cec97ca24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 541,
    "layers": [
        {
            "id": "b67c5fef-c158-4e22-a967-221cec97ca24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 441,
    "xorig": 220,
    "yorig": 270
}