{
    "id": "b64476b9-356c-42ba-af52-59d7aa3a97f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCredits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 214,
    "bbox_left": 53,
    "bbox_right": 1020,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e0ad7d6-7dbb-48bf-8ee8-4e61c11a2d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b64476b9-356c-42ba-af52-59d7aa3a97f0",
            "compositeImage": {
                "id": "0918ec47-0c24-474f-8373-f8b73b7d2517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0ad7d6-7dbb-48bf-8ee8-4e61c11a2d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a36e60-5c65-444a-af6d-2b736eaf5698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0ad7d6-7dbb-48bf-8ee8-4e61c11a2d6c",
                    "LayerId": "a945f832-2121-4d2a-aefd-1ccc133ab2b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 247,
    "layers": [
        {
            "id": "a945f832-2121-4d2a-aefd-1ccc133ab2b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b64476b9-356c-42ba-af52-59d7aa3a97f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1080,
    "xorig": 624,
    "yorig": -90
}