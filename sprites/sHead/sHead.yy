{
    "id": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 440,
    "bbox_left": 13,
    "bbox_right": 434,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2690300-e7ba-4d0e-bd2f-99b8bf7a40fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "edc3703a-253f-4017-b807-7aa89a29add4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2690300-e7ba-4d0e-bd2f-99b8bf7a40fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc4227d-eb70-4602-acc8-857119989bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2690300-e7ba-4d0e-bd2f-99b8bf7a40fd",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        },
        {
            "id": "621a98ef-398a-4235-8dba-0de55b4088b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "b1ce45d4-d272-4c53-961a-2332f9c866a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621a98ef-398a-4235-8dba-0de55b4088b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d9c736-55de-484c-aedb-3ab8b9df393a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621a98ef-398a-4235-8dba-0de55b4088b0",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        },
        {
            "id": "90b94cb5-1e98-4d06-b736-246b577f5e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "d0aeeb3e-def5-427a-a53f-051061c5a5e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b94cb5-1e98-4d06-b736-246b577f5e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0c8e8aa-4fc9-4b58-acb0-d65c8d51f83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b94cb5-1e98-4d06-b736-246b577f5e6b",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        },
        {
            "id": "0af58925-741e-424b-ba3f-29fd3b6a3d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "aa7a9629-51ba-4630-bdfc-16eab087abd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0af58925-741e-424b-ba3f-29fd3b6a3d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e56a141-fac1-4c3b-8c04-3a061261b5f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0af58925-741e-424b-ba3f-29fd3b6a3d95",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        },
        {
            "id": "2310989e-5c29-44ce-87c8-7df8dfac616e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "769e9ca3-5183-4bc8-834c-fe6a1596fa90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2310989e-5c29-44ce-87c8-7df8dfac616e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab953cca-91ff-4bfc-ad2a-73113b591031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2310989e-5c29-44ce-87c8-7df8dfac616e",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        },
        {
            "id": "159f9a93-3c1b-4124-aa94-099ccb70a8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "compositeImage": {
                "id": "f261a08e-93cd-412b-b943-41b4c83dbbc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159f9a93-3c1b-4124-aa94-099ccb70a8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d0e0ce-dceb-4a92-a8f5-ae01cc418802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159f9a93-3c1b-4124-aa94-099ccb70a8b6",
                    "LayerId": "06689c34-b3df-4633-bff7-e80e213eab58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 441,
    "layers": [
        {
            "id": "06689c34-b3df-4633-bff7-e80e213eab58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 441,
    "xorig": 220,
    "yorig": 440
}