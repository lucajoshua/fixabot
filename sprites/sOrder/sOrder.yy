{
    "id": "994b8148-7a61-44f2-8e3e-692e41e22b19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOrder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 327,
    "bbox_left": 9,
    "bbox_right": 195,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dbfdca7-df54-4ae2-9f09-912b7e28ff73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "71b9184a-c7bf-4079-a07e-015e918c98b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dbfdca7-df54-4ae2-9f09-912b7e28ff73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f8588a-4787-494e-9853-61115a5dbdda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dbfdca7-df54-4ae2-9f09-912b7e28ff73",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        },
        {
            "id": "2c41ab69-3bcc-431a-a478-6150ea5c5f9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "4775bc09-77be-4b8c-8329-1536382fbdf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c41ab69-3bcc-431a-a478-6150ea5c5f9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e39c7c6d-0c8e-4631-bc20-7daccebef828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c41ab69-3bcc-431a-a478-6150ea5c5f9a",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        },
        {
            "id": "abad5f64-5472-47e3-b254-47bd7a1066ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "16388597-9c99-4695-b83e-49b6f7e5df14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abad5f64-5472-47e3-b254-47bd7a1066ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97bdc9b9-8e21-4d8f-8b61-ebad756e8b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abad5f64-5472-47e3-b254-47bd7a1066ad",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        },
        {
            "id": "d7492607-aee2-4020-ad0e-7f4401ebe681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "b12af570-3bc1-46a5-9b98-d5d2fcd879d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7492607-aee2-4020-ad0e-7f4401ebe681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d31d9865-cbf4-487d-9020-db0c3638018e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7492607-aee2-4020-ad0e-7f4401ebe681",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        },
        {
            "id": "9f4ca82f-5e73-455e-845c-6be5b77ff194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "be7ead6e-f2ad-48e3-9c4c-ca8fadc81a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4ca82f-5e73-455e-845c-6be5b77ff194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e698718-0522-4d48-be6f-8e880b6b0a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4ca82f-5e73-455e-845c-6be5b77ff194",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        },
        {
            "id": "d2252e84-37cd-4885-b48e-3e354ffe49c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "compositeImage": {
                "id": "af74b1d8-742b-4602-ac90-5c1d1bf6d62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2252e84-37cd-4885-b48e-3e354ffe49c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "842bb3f6-806b-47c3-ac50-84592982da74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2252e84-37cd-4885-b48e-3e354ffe49c4",
                    "LayerId": "7893ac87-d05c-4e42-ae7b-d33177636c6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 328,
    "layers": [
        {
            "id": "7893ac87-d05c-4e42-ae7b-d33177636c6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "994b8148-7a61-44f2-8e3e-692e41e22b19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}