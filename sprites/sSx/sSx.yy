{
    "id": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 2,
    "bbox_right": 241,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33a32c3a-184f-476b-a2da-cf9bb8645fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "fe9406d4-1882-4969-b26f-544aefd3c3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a32c3a-184f-476b-a2da-cf9bb8645fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74c64f0-7ad8-46cf-8016-e9209958d4a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a32c3a-184f-476b-a2da-cf9bb8645fdd",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        },
        {
            "id": "38ab273e-0290-4f31-bd57-a0d8b74e9fec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "a8ddd416-9394-4ff8-a53e-29f95633c947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ab273e-0290-4f31-bd57-a0d8b74e9fec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e44be0a-a301-4eba-90aa-5395bf6822e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ab273e-0290-4f31-bd57-a0d8b74e9fec",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        },
        {
            "id": "caf851c6-4c53-4c48-9c97-2cc87ac4b0af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "d0492005-fee3-4e6c-bd2a-0c8090d69589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf851c6-4c53-4c48-9c97-2cc87ac4b0af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c96cec-f4ae-4410-abd6-7ad5ffaaff9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf851c6-4c53-4c48-9c97-2cc87ac4b0af",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        },
        {
            "id": "b620f671-340c-4422-8233-f25ed850c118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "5e692ba4-8f43-4196-b06d-8c461ad94858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b620f671-340c-4422-8233-f25ed850c118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1795bfd4-4024-41ca-98d1-eb0d466a8c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b620f671-340c-4422-8233-f25ed850c118",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        },
        {
            "id": "570bf63c-ac30-4e99-be16-33bdfbc9ea99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "470be543-7c33-4964-8f96-4f1ed6bd41c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570bf63c-ac30-4e99-be16-33bdfbc9ea99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4983445e-e0ee-461a-b176-c92168027c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570bf63c-ac30-4e99-be16-33bdfbc9ea99",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        },
        {
            "id": "3e9a303c-bf41-4727-a085-8c4cfe2df067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "compositeImage": {
                "id": "0812a460-b12c-45f9-8720-7188f76dafcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9a303c-bf41-4727-a085-8c4cfe2df067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caec2310-773a-4f47-865a-60ecf8e1e44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9a303c-bf41-4727-a085-8c4cfe2df067",
                    "LayerId": "e38b9d93-c965-4f68-8022-f538ff5bb394"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 541,
    "layers": [
        {
            "id": "e38b9d93-c965-4f68-8022-f538ff5bb394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daf8fc9a-3d0f-410c-885f-d89ec31e7641",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 243,
    "yorig": 0
}