{
    "id": "ec018998-02dc-4fc0-aa0d-e168dbfecec9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFullscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a91487f4-6775-469a-bfbe-c75ca5cf8c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec018998-02dc-4fc0-aa0d-e168dbfecec9",
            "compositeImage": {
                "id": "09830012-3bb2-4c20-8472-2517c45600dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a91487f4-6775-469a-bfbe-c75ca5cf8c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8fa156-4d43-42b1-bf70-92cf3acad8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a91487f4-6775-469a-bfbe-c75ca5cf8c72",
                    "LayerId": "74314e6e-af99-4627-95f7-b7a5a1980831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "74314e6e-af99-4627-95f7-b7a5a1980831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec018998-02dc-4fc0-aa0d-e168dbfecec9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}