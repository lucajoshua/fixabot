/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 6036C32A
/// @DnDArgument : "font" "bigFont"
/// @DnDSaveInfo : "font" "04cef3c2-24be-4bf4-94ea-02d3a6086bee"
draw_set_font(bigFont);

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 4A5D3E88
/// @DnDArgument : "halign" "fa_center"
/// @DnDArgument : "valign" "fa_middle"
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 7C1D7C8F
/// @DnDArgument : "x" "559"
/// @DnDArgument : "y" "1122"
/// @DnDArgument : "caption" ""
/// @DnDArgument : "var" "global.robotsAssemlbed"
draw_text(559, 1122,  + string(global.robotsAssemlbed));