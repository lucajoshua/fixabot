{
    "id": "16a2ecd8-38b1-4166-89ae-d24ecdb8fd4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBody",
    "eventList": [
        {
            "id": "e9b775fe-f573-45de-997c-0c13e66aaf63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "16a2ecd8-38b1-4166-89ae-d24ecdb8fd4b"
        },
        {
            "id": "0285b242-eaee-4fa6-a3b4-7835988d1815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16a2ecd8-38b1-4166-89ae-d24ecdb8fd4b"
        },
        {
            "id": "5aef3849-f03a-41f6-ac13-29ed5c6786fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16a2ecd8-38b1-4166-89ae-d24ecdb8fd4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "ca5299a6-b3d7-4137-a68f-fb0d196cf80a",
    "visible": true
}