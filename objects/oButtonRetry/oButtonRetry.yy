{
    "id": "17e2edfe-8b9c-47a0-a5b3-1f947450312d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonRetry",
    "eventList": [
        {
            "id": "f683abf7-1db8-4326-bd41-34f8d1f846d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "17e2edfe-8b9c-47a0-a5b3-1f947450312d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d619d9e-f210-4213-b8fb-d845d6cf1d88",
    "visible": true
}