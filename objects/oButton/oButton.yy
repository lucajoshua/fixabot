{
    "id": "cc9ba824-da4d-4749-9dce-ddadf51aa4b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButton",
    "eventList": [
        {
            "id": "f3c185cd-5f4d-43ab-86bb-46e49b6bb7a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cc9ba824-da4d-4749-9dce-ddadf51aa4b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba0a9551-0e69-487d-8364-c406443c25e3",
    "visible": true
}