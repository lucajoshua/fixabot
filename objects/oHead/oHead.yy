{
    "id": "8d647048-42d4-4238-ba14-4a16352e875c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHead",
    "eventList": [
        {
            "id": "723fbbd2-9049-427a-8cd8-1e82a1a69fdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8d647048-42d4-4238-ba14-4a16352e875c"
        },
        {
            "id": "943af405-59f8-49a4-b7b3-58b177428825",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d647048-42d4-4238-ba14-4a16352e875c"
        },
        {
            "id": "508f5b72-ee5d-47f6-8a27-7ea07697128a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d647048-42d4-4238-ba14-4a16352e875c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "d181220d-2cc4-41be-9dad-8c282dd5bd41",
    "visible": true
}