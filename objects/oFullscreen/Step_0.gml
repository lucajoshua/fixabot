/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Mouse_Pressed
/// @DnDVersion : 1.1
/// @DnDHash : 5D31DA3C
var l5D31DA3C_0;
l5D31DA3C_0 = mouse_check_button_pressed(mb_left);
if (l5D31DA3C_0)
{
	/// @DnDAction : YoYo Games.Collisions.If_Collision_Point
	/// @DnDVersion : 1
	/// @DnDHash : 563217E9
	/// @DnDParent : 5D31DA3C
	/// @DnDArgument : "x" "mouse_x"
	/// @DnDArgument : "y" "mouse_y"
	/// @DnDArgument : "target" "clickedItem"
	/// @DnDArgument : "obj" "all"
	/// @DnDArgument : "notme" "0"
	var l563217E9_0 = collision_point(mouse_x, mouse_y, all, true, 0);
	clickedItem = l563217E9_0;
	if((l563217E9_0))
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1EA5CEDC
		/// @DnDParent : 563217E9
		/// @DnDArgument : "expr" "clickedItem"
		/// @DnDArgument : "var" "clicked_on"
		clicked_on = clickedItem;
	}
}