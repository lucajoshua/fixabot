{
    "id": "ca41d714-b5c3-4df0-8fe1-df66d6c9ddb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFullscreen",
    "eventList": [
        {
            "id": "28a1d9f2-46f2-4142-8eed-c797faaf5830",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ca41d714-b5c3-4df0-8fe1-df66d6c9ddb8"
        },
        {
            "id": "7c42af29-7baa-4b63-8f2f-1c893b497f5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca41d714-b5c3-4df0-8fe1-df66d6c9ddb8"
        },
        {
            "id": "bd740bd3-dda3-464a-b746-3979585a3d25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ca41d714-b5c3-4df0-8fe1-df66d6c9ddb8"
        },
        {
            "id": "2d4b75f1-5267-4374-9e59-8e979989b9c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca41d714-b5c3-4df0-8fe1-df66d6c9ddb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "1e7e960b-1d99-4217-8d65-4a58b9fabc6b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"\"",
            "varName": "clicked_on",
            "varType": 2
        },
        {
            "id": "9b4a26d2-b158-4e7f-a108-f7a13ee41ba3",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "ec018998-02dc-4fc0-aa0d-e168dbfecec9",
    "visible": true
}