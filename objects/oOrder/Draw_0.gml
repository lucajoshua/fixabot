/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 4FEE338A
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 5CFD2405
/// @DnDArgument : "font" "font1"
/// @DnDSaveInfo : "font" "e1cabce6-f7dc-4b56-92cb-29b1aba36ef6"
draw_set_font(font1);

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 39A546BC
/// @DnDArgument : "imageind" "global.parts"
/// @DnDArgument : "spriteind" "sOrder"
/// @DnDSaveInfo : "spriteind" "994b8148-7a61-44f2-8e3e-692e41e22b19"
sprite_index = sOrder;
image_index = global.parts;

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 3919D0FA
/// @DnDArgument : "x" "130"
/// @DnDArgument : "y" "30"
/// @DnDArgument : "caption" ""
/// @DnDArgument : "var" "round(alarm[2]/60)"
draw_text(130, 30,  + string(round(alarm[2]/60)));