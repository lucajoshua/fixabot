/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 35B21693
/// @DnDArgument : "code" "randomize()$(13_10)global.parts = irandom_range(0,5)$(13_10)$(13_10)robotTaken = false;$(13_10)robotEntering = true;$(13_10)global.robotsAssemlbed = 0$(13_10)robotCompleted = false$(13_10)$(13_10)//Alarm for regenerating robot$(13_10)alarm[0] = 0$(13_10)$(13_10)//Alarm for entering animation$(13_10)alarm[1] = 0$(13_10)$(13_10)//Alarm for game timeout$(13_10)alarm[2] = 1500 "
randomize()
global.parts = irandom_range(0,5)

robotTaken = false;
robotEntering = true;
global.robotsAssemlbed = 0
robotCompleted = false

//Alarm for regenerating robot
alarm[0] = 0

//Alarm for entering animation
alarm[1] = 0

//Alarm for game timeout
alarm[2] = 1500