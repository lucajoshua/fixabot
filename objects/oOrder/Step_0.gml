/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 0EE75EEB
/// @DnDArgument : "font" "font0"
/// @DnDSaveInfo : "font" "75725033-f744-4af5-bd05-cb0f86dcb095"
draw_set_font(font0);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 621B779E
/// @DnDArgument : "code" "oHead_y = 640$(13_10)oBody_y = 896$(13_10)oLegs_y = 1696$(13_10)oDx_y = 608$(13_10)oSx_y = 608 $(13_10)oDx_x = 768$(13_10)oSx_x = 320 $(13_10)$(13_10)if(alarm[0]<0 && robotTaken = true){$(13_10)	global.robotsAssemlbed += 1$(13_10)	alarm[0] = 90	$(13_10)}$(13_10)$(13_10)if(alarm[1]<0 && robotEntering = true){	$(13_10)	oHead.image_xscale = 0.2$(13_10)	oHead.image_yscale = 0.2$(13_10)	oBody.image_xscale = 0.2$(13_10)	oBody.image_yscale = 0.2$(13_10)	oLegs.image_xscale = 0.2$(13_10)	oLegs.image_yscale = 0.2$(13_10)	oDx.image_xscale = 0.2$(13_10)	oDx.image_yscale = 0.2$(13_10)	oSx.image_xscale = 0.2$(13_10)	oSx.image_yscale = 0.2$(13_10)	$(13_10)	oHead.y = 1300$(13_10)	oBody.y = 1344$(13_10)	oLegs.y = 1500$(13_10)	oDx.y = 1260$(13_10)	oDx.x = 605$(13_10)	oSx.y = 1260$(13_10)	oSx.x = 465$(13_10)	alarm[1] = 120	$(13_10)}$(13_10)$(13_10)if(alarm[2]<0){$(13_10)	// GAME ENDING	$(13_10)}$(13_10) $(13_10)	$(13_10)if(oHead.theme == global.parts && oBody.theme == global.parts && oLegs.theme == global.parts && oDx.theme == global.parts && oSx.theme == global.parts){$(13_10)	//game_restart()$(13_10)	/// NEXT ROBOT HERE $(13_10)	robotTaken = true$(13_10)	if(robotTaken = true){$(13_10)		audio_play_sound(mRullo2,1,0)$(13_10)		oHead.y += -50$(13_10)		oBody.y += -50$(13_10)		oLegs.y += -50$(13_10)		oDx.y += -50$(13_10)		oSx.y += -50 $(13_10)	}$(13_10)}$(13_10)$(13_10)animationTime = 0.02$(13_10)$(13_10)$(13_10)if(!robotTaken){$(13_10)	var lay_id = layer_get_id("Background");$(13_10)	var back_id = layer_background_get_id(lay_id);$(13_10) if(robotEntering){ $(13_10)	layer_background_speed(back_id, 4);$(13_10)	 //audio_play_sound(mRullo2,1,0)$(13_10)	 //HEAD ANIM$(13_10)	oHead.image_xscale += animationTime$(13_10)	oHead.image_yscale += animationTime$(13_10)	oHead.y += -10$(13_10)	if (oHead.image_xscale >= 1){$(13_10)		oHead.image_xscale = 1$(13_10)	}$(13_10)	if (oHead.image_yscale >= 1){$(13_10)		oHead.image_yscale = 1$(13_10)	}$(13_10)	if (oHead.y <= oHead_y){$(13_10)		oHead.y = oHead_y$(13_10)	}$(13_10)	$(13_10)	 //BODY ANIM$(13_10)	oBody.image_xscale += animationTime$(13_10)	oBody.image_yscale += animationTime$(13_10)	oBody.y += -10$(13_10)	if (oBody.image_xscale >= 1){$(13_10)		oBody.image_xscale = 1$(13_10)	}$(13_10)	if (oBody.image_yscale >= 1){$(13_10)		oBody.image_yscale = 1$(13_10)	}$(13_10)	if (oBody.y <= oBody_y){$(13_10)		oBody.y = oBody_y$(13_10)	}$(13_10)	$(13_10)	 //LEGS ANIM$(13_10)	oLegs.image_xscale += animationTime$(13_10)	oLegs.image_yscale += animationTime$(13_10)	oLegs.y += 10$(13_10)	if (oLegs.image_xscale >= 1){$(13_10)		oLegs.image_xscale = 1$(13_10)	}$(13_10)	if (oLegs.image_yscale >= 1){$(13_10)		oLegs.image_yscale = 1$(13_10)	}$(13_10)	if (oLegs.y >= oLegs_y){$(13_10)		oLegs.y = oLegs_y$(13_10)	}$(13_10)	$(13_10)	 //DX ANIM$(13_10)	oDx.image_xscale += animationTime$(13_10)	oDx.image_yscale += animationTime$(13_10)	oDx.y += -10$(13_10)	oDx.x += 10$(13_10)	if (oDx.image_xscale >= 1){$(13_10)		oDx.image_xscale = 1$(13_10)	}$(13_10)	if (oDx.image_yscale >= 1){$(13_10)		oDx.image_yscale = 1$(13_10)	}$(13_10)	if (oDx.y <= oDx_y){$(13_10)		oDx.y = oDx_y$(13_10)	}$(13_10)	if (oDx.x >= oDx_x){$(13_10)		oDx.x = oDx_x$(13_10)	}$(13_10)	$(13_10)	 //SX ANIM$(13_10)	oSx.image_xscale += animationTime$(13_10)	oSx.image_yscale += animationTime$(13_10)	oSx.y += -10$(13_10)	oSx.x += -10$(13_10)	if (oSx.image_xscale >= 1){$(13_10)		oSx.image_xscale = 1$(13_10)	}$(13_10)	if (oSx.image_yscale >= 1){$(13_10)		oSx.image_yscale = 1$(13_10)	}$(13_10)	if (oSx.y <= oSx_y){$(13_10)		oSx.y = oSx_y$(13_10)	}$(13_10)	if (oSx.x <= oSx_x){$(13_10)		oSx.x = oSx_x$(13_10)	}$(13_10) } else {$(13_10)	// audio_stop_sound(mRullo2)$(13_10)	  layer_background_speed(back_id,0)$(13_10) }$(13_10)}	 "
oHead_y = 640
oBody_y = 896
oLegs_y = 1696
oDx_y = 608
oSx_y = 608 
oDx_x = 768
oSx_x = 320 

if(alarm[0]<0 && robotTaken = true){
	global.robotsAssemlbed += 1
	alarm[0] = 90	
}

if(alarm[1]<0 && robotEntering = true){	
	oHead.image_xscale = 0.2
	oHead.image_yscale = 0.2
	oBody.image_xscale = 0.2
	oBody.image_yscale = 0.2
	oLegs.image_xscale = 0.2
	oLegs.image_yscale = 0.2
	oDx.image_xscale = 0.2
	oDx.image_yscale = 0.2
	oSx.image_xscale = 0.2
	oSx.image_yscale = 0.2
	
	oHead.y = 1300
	oBody.y = 1344
	oLegs.y = 1500
	oDx.y = 1260
	oDx.x = 605
	oSx.y = 1260
	oSx.x = 465
	alarm[1] = 120	
}

if(alarm[2]<0){
	// GAME ENDING	
}
 
	
if(oHead.theme == global.parts && oBody.theme == global.parts && oLegs.theme == global.parts && oDx.theme == global.parts && oSx.theme == global.parts){
	//game_restart()
	/// NEXT ROBOT HERE 
	robotTaken = true
	if(robotTaken = true){
		audio_play_sound(mRullo2,1,0)
		oHead.y += -50
		oBody.y += -50
		oLegs.y += -50
		oDx.y += -50
		oSx.y += -50 
	}
}

animationTime = 0.02


if(!robotTaken){
	var lay_id = layer_get_id("Background");
	var back_id = layer_background_get_id(lay_id);
 if(robotEntering){ 
	layer_background_speed(back_id, 4);
	 //audio_play_sound(mRullo2,1,0)
	 //HEAD ANIM
	oHead.image_xscale += animationTime
	oHead.image_yscale += animationTime
	oHead.y += -10
	if (oHead.image_xscale >= 1){
		oHead.image_xscale = 1
	}
	if (oHead.image_yscale >= 1){
		oHead.image_yscale = 1
	}
	if (oHead.y <= oHead_y){
		oHead.y = oHead_y
	}
	
	 //BODY ANIM
	oBody.image_xscale += animationTime
	oBody.image_yscale += animationTime
	oBody.y += -10
	if (oBody.image_xscale >= 1){
		oBody.image_xscale = 1
	}
	if (oBody.image_yscale >= 1){
		oBody.image_yscale = 1
	}
	if (oBody.y <= oBody_y){
		oBody.y = oBody_y
	}
	
	 //LEGS ANIM
	oLegs.image_xscale += animationTime
	oLegs.image_yscale += animationTime
	oLegs.y += 10
	if (oLegs.image_xscale >= 1){
		oLegs.image_xscale = 1
	}
	if (oLegs.image_yscale >= 1){
		oLegs.image_yscale = 1
	}
	if (oLegs.y >= oLegs_y){
		oLegs.y = oLegs_y
	}
	
	 //DX ANIM
	oDx.image_xscale += animationTime
	oDx.image_yscale += animationTime
	oDx.y += -10
	oDx.x += 10
	if (oDx.image_xscale >= 1){
		oDx.image_xscale = 1
	}
	if (oDx.image_yscale >= 1){
		oDx.image_yscale = 1
	}
	if (oDx.y <= oDx_y){
		oDx.y = oDx_y
	}
	if (oDx.x >= oDx_x){
		oDx.x = oDx_x
	}
	
	 //SX ANIM
	oSx.image_xscale += animationTime
	oSx.image_yscale += animationTime
	oSx.y += -10
	oSx.x += -10
	if (oSx.image_xscale >= 1){
		oSx.image_xscale = 1
	}
	if (oSx.image_yscale >= 1){
		oSx.image_yscale = 1
	}
	if (oSx.y <= oSx_y){
		oSx.y = oSx_y
	}
	if (oSx.x <= oSx_x){
		oSx.x = oSx_x
	}
 } else {
	// audio_stop_sound(mRullo2)
	  layer_background_speed(back_id,0)
 }
}