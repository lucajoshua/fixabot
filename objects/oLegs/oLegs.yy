{
    "id": "5e7a8000-ce37-440f-9e03-7ba0b9ad088b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLegs",
    "eventList": [
        {
            "id": "1e60c084-9df8-40af-b7f8-3f746c4d586e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5e7a8000-ce37-440f-9e03-7ba0b9ad088b"
        },
        {
            "id": "f4549fe0-ffda-48f3-bb3e-e02f23abddb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5e7a8000-ce37-440f-9e03-7ba0b9ad088b"
        },
        {
            "id": "621f053e-730c-453c-b67c-3a08dc471c36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e7a8000-ce37-440f-9e03-7ba0b9ad088b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "cd67f00c-4a43-4274-a959-7d30cf8f65e9",
    "visible": true
}