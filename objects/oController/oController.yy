{
    "id": "6c5d5cd1-35f2-4ac6-ab65-8d04399f9a13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oController",
    "eventList": [
        {
            "id": "7b0d6c5b-0e50-4398-95bd-6681ecbd2188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c5d5cd1-35f2-4ac6-ab65-8d04399f9a13"
        },
        {
            "id": "520ddcce-66db-42f5-86db-9bb3801d0e8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "6c5d5cd1-35f2-4ac6-ab65-8d04399f9a13"
        },
        {
            "id": "3a381055-6e04-43f0-b353-94aa66aa2061",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c5d5cd1-35f2-4ac6-ab65-8d04399f9a13"
        },
        {
            "id": "2cdd409d-5615-44d3-aa29-7a16a709a8a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c5d5cd1-35f2-4ac6-ab65-8d04399f9a13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}