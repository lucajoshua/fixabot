{
    "id": "e1cabce6-f7dc-4b56-92cb-29b1aba36ef6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c10a9cc7-ea31-483e-986e-281ed06d75ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3c822cab-6297-455a-93d3-3dd1511c2159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 144,
                "y": 306
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2d54c0cc-cf5a-450b-926a-993eedd3a27a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 124,
                "y": 306
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "10aff2ee-8d64-4654-90cd-fc488aa2db97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 0,
                "shift": 36,
                "w": 35,
                "x": 87,
                "y": 306
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7569912a-bd88-4a87-a082-205b22c0486d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 54,
                "y": 306
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bdfb6368-e2b4-4984-85bf-afe903cfdf24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 3,
                "shift": 57,
                "w": 50,
                "x": 2,
                "y": 306
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "dd4e316a-16ad-44a1-9bab-f60585afe9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 2,
                "shift": 43,
                "w": 40,
                "x": 453,
                "y": 230
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "062de8a2-b79a-47d2-a414-d0fc2925dae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 443,
                "y": 230
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b5d70bdd-ffa7-4044-b2e8-6ef4ff2d2494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 425,
                "y": 230
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "25f59b5f-42ee-48df-aeb3-fe5c6cb36e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 407,
                "y": 230
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "88aabbc9-3ed5-44db-9ba3-e197e061309d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 154,
                "y": 306
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b486072d-73df-4bea-8349-1d4a0a1a3719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 374,
                "y": 230
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "acf342b5-99ba-4426-8a9e-349983f64d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 330,
                "y": 230
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "92eaa7ed-5f33-409b-b1e2-4dbfb88a2689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 310,
                "y": 230
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4be345de-39ac-48ed-8b79-4b6adcf11ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 300,
                "y": 230
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "26650caa-e7eb-4d0c-ac58-966a9fd21200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 280,
                "y": 230
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9067cf74-709c-4abc-b0d3-798f9e65e94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 247,
                "y": 230
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7a0f882c-ff81-41f5-9984-d1e1a02c3c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 6,
                "shift": 36,
                "w": 18,
                "x": 227,
                "y": 230
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "04b3b81d-f5f8-4146-9f5c-e53aae108a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 1,
                "shift": 36,
                "w": 32,
                "x": 193,
                "y": 230
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d9ed5f82-f31a-4efc-a5ca-d93c094ffa26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 160,
                "y": 230
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f8d44db6-769e-4545-aba9-d047b5c9f04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 36,
                "w": 33,
                "x": 125,
                "y": 230
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0d990223-39d7-4f14-bc2b-440edd7ecb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 340,
                "y": 230
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c00c0c5d-c884-4602-9ee3-762ae01b55d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 177,
                "y": 306
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c636ae1b-c7c9-476e-b9c8-6e80fc7af09b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 210,
                "y": 306
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0783daa8-e85b-49dd-96f9-be0d7833bc0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 242,
                "y": 306
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2cc0c64c-ac56-4baa-803e-6a35f7500d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 2,
                "y": 458
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6c46927a-6ab5-4665-baff-4454c3b7f8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 490,
                "y": 382
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "09e78dec-4dda-43a7-8591-57a762fb6fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 480,
                "y": 382
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "63d8d447-6473-4c87-abd0-16b1cefab029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 447,
                "y": 382
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2c11553d-73ef-4039-b779-655f6d112221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 414,
                "y": 382
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1bbc5f93-e2d2-4943-9134-d329626b96c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 381,
                "y": 382
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "566c81a6-ae5d-44dc-9eea-924d8fc3cd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 348,
                "y": 382
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8acd5700-c00c-4cbb-83c0-335b4e2c4405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 3,
                "shift": 65,
                "w": 60,
                "x": 286,
                "y": 382
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "055eb44b-0911-4379-9f61-19aa95ed547e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": -1,
                "shift": 43,
                "w": 44,
                "x": 240,
                "y": 382
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "832ebb9e-34a2-4d50-a16e-e9ff1e083b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 4,
                "shift": 43,
                "w": 36,
                "x": 202,
                "y": 382
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ced5a9cd-5102-4ceb-afa9-0027c5f81fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 3,
                "shift": 46,
                "w": 41,
                "x": 159,
                "y": 382
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "86a3afff-5d5e-471a-b50e-c7fe88085916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 4,
                "shift": 46,
                "w": 39,
                "x": 118,
                "y": 382
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "58d4426a-411e-45f4-a6bc-8fde5c1560d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 5,
                "shift": 43,
                "w": 35,
                "x": 81,
                "y": 382
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e79e24be-68e6-4104-82b7-4c8e86152cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 5,
                "shift": 39,
                "w": 32,
                "x": 47,
                "y": 382
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1dfc11d4-dcb6-48aa-9560-dc02649dd2fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 3,
                "shift": 50,
                "w": 43,
                "x": 2,
                "y": 382
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "869046fe-d767-48b5-8160-3e8b1899fffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 5,
                "shift": 46,
                "w": 37,
                "x": 434,
                "y": 306
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cc4fd419-6720-4c0c-933a-430ee5750e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 424,
                "y": 306
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a54eb36c-ff8b-427d-a94e-b3a80abaefd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 1,
                "shift": 32,
                "w": 27,
                "x": 395,
                "y": 306
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "752a60a2-6f41-4f0b-bf85-cc4a5986db5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 4,
                "shift": 43,
                "w": 39,
                "x": 354,
                "y": 306
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8aba730b-0995-40b7-bd41-c977a6e042ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 30,
                "x": 322,
                "y": 306
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2b64eb56-792b-4819-a1df-6b8348095f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 4,
                "shift": 53,
                "w": 45,
                "x": 275,
                "y": 306
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b2bf95ef-6af1-431c-aae4-c9af668fc766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 4,
                "shift": 46,
                "w": 37,
                "x": 86,
                "y": 230
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d7335937-5ff8-4d3c-b6bf-d8e65b7b90ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 40,
                "y": 230
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "65ef5696-35d5-4ceb-8bae-2d16b31ffda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 4,
                "shift": 43,
                "w": 36,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9a5c853e-35d9-4603-83e6-e745fdbc0a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 285,
                "y": 78
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5317c1cd-b68b-45fc-8e67-fe9a05ce07c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 5,
                "shift": 46,
                "w": 41,
                "x": 227,
                "y": 78
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1cca2426-668e-477c-bd04-70956e155223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 187,
                "y": 78
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3ba0595a-be8c-4736-9403-10d273e9e451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 148,
                "y": 78
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dfbe2c6f-cce5-4768-a12c-cb44f794c8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 5,
                "shift": 46,
                "w": 37,
                "x": 109,
                "y": 78
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cbbc8a09-e721-46c4-8ad8-58e37bb7d2b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 64,
                "y": 78
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ba5a0446-c5d6-4f23-8aaf-167f086b4b86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 0,
                "shift": 60,
                "w": 60,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "15327a4e-704b-4dd0-a385-1cc134b85d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "921eef96-d3d9-4010-a674-7c77e28f3629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 363,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f6aa872f-80ee-4d5a-b359-420dd6444018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "427813a1-a3f7-4aa4-b018-59009a8993ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 18,
                "w": 13,
                "x": 270,
                "y": 78
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "55445318-b60c-4e79-a0f7-ef6e11f78f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "34aa0043-c2c1-4a8b-9f98-eedb9f8e149f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 1,
                "shift": 18,
                "w": 13,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "35ad66b2-f8c0-433f-a64d-be1cb8984535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "686495b3-f2e8-4c47-9b77-de3e8888f557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e0aafc98-02d2-4c78-b0e4-a25f22f2495b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 21,
                "w": 13,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "21a73d3e-4ebd-4484-bb6a-fcd2c97859df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "349387cd-efaa-4b10-aa62-71bf06ed3d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b70fc9b0-ec90-4c7f-9104-e94ba5d44883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 2,
                "shift": 32,
                "w": 30,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb6dfab7-2f58-4978-8fef-3c00fac83887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 29,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "181efbcf-1062-4ea8-ac1d-2554c449ec56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5ddc2fea-6de1-4b9d-aee9-9f15a0066cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 20,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "76652746-d19a-4da8-a845-c9f647a658bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 30,
                "x": 333,
                "y": 78
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3e938523-c011-4e0e-ac3d-6c71f801ec2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 147,
                "y": 154
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "95c88e00-2b3b-4da4-8199-c3e6f34bd0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 365,
                "y": 78
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cc789406-26b0-41d4-9aab-50e8b39e9fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -3,
                "shift": 14,
                "w": 13,
                "x": 430,
                "y": 154
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ab7ac3e6-3c18-44c0-9756-d7255dc394eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 4,
                "shift": 32,
                "w": 28,
                "x": 400,
                "y": 154
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "43254f40-bfc1-4b0c-8ad2-91ed0688791b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 392,
                "y": 154
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4787246f-7db7-4039-952b-cddf522b216a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 4,
                "shift": 53,
                "w": 46,
                "x": 344,
                "y": 154
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c59ce714-2bab-4711-9e4c-ac3efa95e8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 314,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7337766e-a4e8-4eea-bbb4-7386023eb9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 280,
                "y": 154
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dac0dfc7-ba17-406d-9515-4da340a5638c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 30,
                "x": 248,
                "y": 154
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "61cf5c35-f5ef-4124-930f-2cec800ebc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 29,
                "x": 217,
                "y": 154
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "60e18075-f49b-45a5-9f11-f5b6efd40272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 4,
                "shift": 21,
                "w": 19,
                "x": 196,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "476cef50-6de8-495b-b63c-95a1d1d28807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 1,
                "shift": 32,
                "w": 29,
                "x": 445,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "29e1d863-9410-4269-8cd3-e5569a811375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "671745f3-3a31-45f9-a1a8-cced965ddbe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 4,
                "shift": 36,
                "w": 27,
                "x": 118,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "458b1c61-55ea-412a-9d0f-243f5098b9a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 84,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fe06736c-82bf-44fb-96ae-18410eaa7f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 0,
                "shift": 46,
                "w": 46,
                "x": 36,
                "y": 154
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bfdfc42e-2909-4ca2-895a-a296ac484b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ff29b23c-f97e-4fee-bdb9-8277b23efd1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 455,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d0796cfd-bbc1-470b-a14f-f4503dbd5f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 423,
                "y": 78
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d6b5541e-493b-467b-9328-b8211b83928a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 402,
                "y": 78
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b1d5cf5b-e7f5-4b67-af5d-f924d997e488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 5,
                "shift": 17,
                "w": 6,
                "x": 394,
                "y": 78
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0b10e754-60f7-44b4-9293-9c8af6ea4ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 373,
                "y": 78
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f74a4b1f-00d7-4bf6-be49-7a586de6b9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 35,
                "y": 458
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3555046c-3bf6-4835-9fa3-de2f9451c9fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 74,
                "offset": 10,
                "shift": 39,
                "w": 19,
                "x": 70,
                "y": 458
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f36fcb1e-1d69-438d-83eb-aba5ee2d34e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "4fe7094d-5756-4901-a131-63228ff800d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "be85924c-725f-4cf6-ba94-c7d9bf49a00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "58e497d9-f1b5-4189-8921-456998834313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 902
        },
        {
            "id": "786fa0d7-2ff7-4db6-8b15-670c058674fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 913
        },
        {
            "id": "a7be6309-9e8f-4446-a2b1-dac365f12009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 916
        },
        {
            "id": "1d6d4c4b-d459-4703-8eaa-6f0cfd2fee3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 923
        },
        {
            "id": "7e7cdcd6-263d-4e9e-8c7f-5875da9df257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "a1fbcc41-cc15-4f94-b2ab-47925109ded1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "48c8d54e-dd8e-40ba-bd1f-470b7e909d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "d6de228e-3d92-492b-9a58-ec8ea6839416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 49
        },
        {
            "id": "232b0067-840d-4df7-a37b-7053b7d0b663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "43ecde39-3819-48e7-af81-ac9e6db56472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "d70d6216-a4e6-4275-ad26-496a6fa2b40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 86
        },
        {
            "id": "f395edda-66e1-42a5-bacd-d057300274f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "d5a9f98e-dbcc-474c-889c-e56a1de3f0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "dd3c0ff2-c40b-4b24-84d8-0e635924e005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "ebc2db7d-13f3-42e1-b656-cf84092461f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "26f6007d-322b-40e4-94d7-f540e33f0add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "bea07d12-de33-4c92-b86e-2949773513d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 160
        },
        {
            "id": "0ff3949e-93d7-498d-be27-67d60b87812b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8217
        },
        {
            "id": "009ff1b2-9162-4ad4-9a5f-61ed315cc1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "627cda5d-ee12-4d14-bec2-9d84594f9681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "47c29a32-44ee-4565-95a5-5c064e905551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "e837d720-de3b-42c9-81da-821e14d40d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "5b1daf4a-58d3-4662-b1b5-077fbe0909e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "cb1b296f-b6c4-4297-bbb5-d969475db3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "fc372ab6-cba7-419d-8a57-3edd6445b768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 87
        },
        {
            "id": "95883fff-4fab-403e-a00a-a0c10d7ccf10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "05cc7ea5-b8c6-44eb-91e1-f13513cce16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "e7ee703d-c020-424c-b33e-052889bf87e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "053391c7-bcb6-4e30-aacb-8d299430af97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "77133bdb-d70b-44b9-b9be-38213777bf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "5e27c780-d1ee-46b7-9610-f2dca687eeb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "b3817629-de02-4386-ba93-845d5ece7500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "adba677c-0f77-4126-a188-5e44b317e674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 65
        },
        {
            "id": "cf18cfa2-096a-4eb3-9bf8-ed1268e3c793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "a4feee3f-0530-4657-a5b5-547a53ed44ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "c675211f-ba5c-4272-991b-6ca5c1b8ba6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "454e7414-b9c2-4fef-8b10-ee239217509d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "4e92180b-e444-4b27-b900-86e6143a3d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "a0d9844d-b004-46b9-9b55-f61f3fdfc8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "550dffdf-24c3-478f-ac0a-8ba1d18b47a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "c1048448-6a1b-4bf3-99bb-703a78ecb829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "08c7ce7c-ff15-4ba7-a22f-cfd3bc4508ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "b0099d81-0c1d-45b4-a569-cae57e3cffac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "638af1ec-70ba-483d-a549-f522ecedef31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "1532cdad-5de3-4a13-874c-247fae7feef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 65
        },
        {
            "id": "f814aa4c-7417-4306-aa41-6e1619dba63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "9ae46720-2f32-41ad-8209-b4e19dea93c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "4bdc8518-eb8c-42e4-bb9b-7dd63316676b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "aa9cc03b-3777-4431-97e7-afde37cb44fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "e41b4dbe-8f3d-428d-81d2-01e5b30d8aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "6b43d74c-acca-4ee8-a558-e539e92b8e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 111
        },
        {
            "id": "85665180-c37f-4a6e-ac4b-5ea4dc9c02ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "14fc79a2-1969-4966-a15c-0b92ef4dde9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "1141c3af-4e28-4105-9bf8-f2e2a6f15eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "bc845854-1f60-4b15-b16c-a0610bffa05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "05e83661-ef85-44da-850d-519be18249ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "b9c952f9-7ca8-4481-a4da-1a0dc2302ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "e921fcfd-18fd-4559-baeb-38c49065f447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 173
        },
        {
            "id": "34eb7106-407b-4cad-be6a-5f621e9f397d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 894
        },
        {
            "id": "39dea043-d1ad-43be-b8cc-2b40f5ec7457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "3d991aab-a5be-4a1b-885e-3ac0be98a305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "a42c04ae-0d39-4963-969e-05a9bf1614bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "60c7a346-0cf1-466d-b909-fba845e31f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "250f877f-7689-4650-a6f6-99e898f16b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "84490460-5a7e-4836-adfa-61f6c14575d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 65
        },
        {
            "id": "16dc94b6-9252-47ed-a8d1-3719d2ae5caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 97
        },
        {
            "id": "fbfc11d9-7274-4121-a10a-946fbc14862a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "fb4bf793-b2e7-4fd0-80b8-943cb9f25a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "b588803d-eb4e-4073-999d-7a0f8b05fc98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "4ecb2c7e-107c-49c6-b117-6ccdc54ac359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "73bbb54f-b6e8-41f3-96eb-9a3e2732917e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "69b6ca19-0b00-4fc6-93a7-42423585967b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "da3d88c8-420d-4b6b-8d22-cb9be09b018a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "4d9dfd08-3843-4607-b3c6-8fbd54c086cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "0f20b56e-7069-4477-bd63-a22617cc68e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "fd32ea69-d30a-49a2-9131-fbe79a1f0472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "aa959fa7-60ae-435e-800c-68408b493d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "d1ae20af-8d88-440c-aac5-0d2cf7ed034e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "79e61ab5-f409-4b75-b10c-c26c6a509c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "a4c29a77-a259-461b-afa2-cdd9be61f878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "90ee0cb0-2957-4e1f-87b4-72236e53ef9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "a02000e2-ce7c-4edb-b8d4-aada88693048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "07e28c5c-0ec8-4faf-9fb5-e72b44c52056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "51fd8001-37be-4b1a-a5a5-bc519de654da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "f891bd18-297a-423a-80c5-b6d27a961b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "5017823b-a7f9-4d38-87ba-f9ebfeb3bccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "ffec1233-8f20-4f0d-8b9a-26d47b993a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "7e08744d-4209-463b-bd90-c693a7a420ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "055541c7-2914-4a72-b6e3-34c5a01c4fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "26a23f05-8a4f-4951-b4a7-f923831d27ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 44
        },
        {
            "id": "c549570e-33d0-4430-a679-775c520e4255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "bc25addc-44e5-4c6d-b00b-263aee043338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 46
        },
        {
            "id": "a2bbdb87-2171-4e2a-b1a4-79ecfe1ab228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "b93a8764-d6e9-4670-97a2-e05c40258904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "72c1a6ad-9170-4568-8181-f09588aae2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 65
        },
        {
            "id": "5c0aa3ef-466f-41fa-bf52-77e32beb8a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 97
        },
        {
            "id": "1f81e302-92ba-4839-8d97-7f3859c9bf6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "6ebf48f9-21d3-4b8b-b931-4cddd63f9b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "df65f172-5409-4ff6-beb3-f4bde758cd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "26c6b035-3fd8-4832-8477-226e6a0b7e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 112
        },
        {
            "id": "6b193fe5-26a3-4258-b453-e2080e086555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "43a1c130-842e-45c9-9404-531fc3bb5454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "0a3e6efd-85f8-40b3-9d19-d0cbc5081c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "e75609dd-b239-4e32-abe7-c89e89022feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "c2307551-a38b-4629-9253-8e4b1eb0e560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "0cb9100b-0b55-4f1d-a44b-93fa2c647199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "bd943c99-81ef-48e9-9992-695723fd9aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "6e5366c2-a30c-4dbe-9ba5-e47049210b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "9dff7020-5238-4c04-ab58-8bdbdd1313cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "6b6fad8f-179a-4af9-a4f7-4f75000c4c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "dd705a4f-4e95-400b-9e21-bdd583b5821e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "ccd171b4-4750-441e-a696-15a3810221e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "bfbd4ad9-b045-48cf-aebf-73433a1c5126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "5fd1b497-08c0-411f-8460-31646e008a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "fbce7c69-9e85-4033-945b-e612bb0501d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "799e32bb-d554-48e6-a057-41607a5f43cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "ee3947b1-53bb-4f11-83a7-721a793aa10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}