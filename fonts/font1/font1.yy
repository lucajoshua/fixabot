{
    "id": "e1cabce6-f7dc-4b56-92cb-29b1aba36ef6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Roboto",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1d28f396-98be-4043-8d0a-c19bfba2e94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 75,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7c1f5512-8461-4917-a3fc-911928c1998e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 75,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 58,
                "y": 156
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "339838db-02e7-4961-b63e-7dcdac6100ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 75,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 39,
                "y": 156
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "adc59d87-d268-461f-bfaf-1a6de4080e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 75,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 2,
                "y": 156
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8ed7362f-0533-4b0e-80fc-505dfb1675a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 989,
                "y": 79
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f54b7c2e-d6a2-4b18-8e01-a5f3bdfcccc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 75,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 944,
                "y": 79
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "93ebb9b7-05cf-49bb-892d-bc27776f0327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 75,
                "offset": 2,
                "shift": 42,
                "w": 40,
                "x": 902,
                "y": 79
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a7ece488-7386-4588-a4d4-d0612ffd8d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 75,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 892,
                "y": 79
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0acb2d96-83ee-42bb-a71c-44beaa977b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 75,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 871,
                "y": 79
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7d59b326-519a-47f6-96ee-b6a398782224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 75,
                "offset": 1,
                "shift": 23,
                "w": 19,
                "x": 850,
                "y": 79
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "91ff9f96-e958-4b6e-b4b2-ef6dd9c2b8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 75,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 71,
                "y": 156
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "62d83179-ba60-4f92-8ac0-d03450155f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 75,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 816,
                "y": 79
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fb3ab023-7023-4ea3-ba11-21b9a311aac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 75,
                "offset": 1,
                "shift": 16,
                "w": 11,
                "x": 769,
                "y": 79
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8fea2734-876a-4afa-bb04-1aaac970e2d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 75,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 748,
                "y": 79
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8c1326b9-a9d0-4c55-a2b5-bda5570be90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 75,
                "offset": 3,
                "shift": 19,
                "w": 12,
                "x": 734,
                "y": 79
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d09315e5-ff86-4156-8398-696063522fe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 75,
                "offset": -1,
                "shift": 24,
                "w": 25,
                "x": 707,
                "y": 79
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b9552e23-1a17-4eaf-884f-b6dd6e94baae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 673,
                "y": 79
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ecd24cb1-c971-4f46-9771-5ebf308dddfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 75,
                "offset": 5,
                "shift": 37,
                "w": 21,
                "x": 650,
                "y": 79
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4a747544-0d01-4189-bfe4-c0c6067bcefc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 615,
                "y": 79
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e3c61b68-bea2-4d07-8b0d-d736c7bfaa46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 581,
                "y": 79
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8aea9337-73b5-4c76-8a6c-da514552b40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 75,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 545,
                "y": 79
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "af9e44d2-a3c4-4551-b200-d41a08bdbb49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 75,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 782,
                "y": 79
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1bde68d3-e041-4b9b-b00b-f357be2aed56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 75,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 102,
                "y": 156
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "076e7a94-6d70-4557-ac86-275a664ace4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 75,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 136,
                "y": 156
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b1f2efff-43c4-4428-b761-6dd026a3dfef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 172,
                "y": 156
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e113978f-8133-4c9b-9d35-a3373a269a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 884,
                "y": 156
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b58a2818-e034-4678-9011-a630be6b9e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 75,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 870,
                "y": 156
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d57d2dd7-9682-4a35-a684-7d99d3dc5949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 75,
                "offset": 1,
                "shift": 17,
                "w": 13,
                "x": 855,
                "y": 156
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ee3f135a-9f68-4427-bb06-d72993b83175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 75,
                "offset": 1,
                "shift": 33,
                "w": 28,
                "x": 825,
                "y": 156
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0fb4c464-3f3b-49cc-9c15-871dc6a9727c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 75,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 794,
                "y": 156
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3784210c-b253-42de-8601-cda16d6dd159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 75,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 764,
                "y": 156
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c319be78-eb35-4fef-938d-19be155542b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 75,
                "offset": 1,
                "shift": 32,
                "w": 29,
                "x": 733,
                "y": 156
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1ec6bf46-20fb-4793-8bf6-94bb23ffb452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 75,
                "offset": 2,
                "shift": 57,
                "w": 53,
                "x": 678,
                "y": 156
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "352cb328-7505-4e96-828e-bbb50a49614b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 75,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 633,
                "y": 156
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fbe22b3d-3ae6-4d1b-885f-fd77981b34dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 75,
                "offset": 4,
                "shift": 41,
                "w": 34,
                "x": 597,
                "y": 156
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "24260c01-2025-484e-9a3e-6e31cabb42ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 75,
                "offset": 2,
                "shift": 42,
                "w": 38,
                "x": 557,
                "y": 156
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ad25fc6a-6c2e-4d6b-986b-753fef09c1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 75,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 520,
                "y": 156
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3db6f597-5231-459a-971c-255f312f3f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 75,
                "offset": 4,
                "shift": 36,
                "w": 31,
                "x": 487,
                "y": 156
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f5612bda-39b1-4d9d-8f26-5091a5b1b272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 75,
                "offset": 4,
                "shift": 35,
                "w": 30,
                "x": 455,
                "y": 156
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "faebe539-57b3-48a8-bcf3-3b215eb2220b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 75,
                "offset": 2,
                "shift": 44,
                "w": 39,
                "x": 414,
                "y": 156
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dcbeec91-782b-4fdb-9bfa-493493b6e1ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 75,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 374,
                "y": 156
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fbd9508c-2176-495d-87a8-3ebd150071bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 75,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 361,
                "y": 156
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ff2918dd-bd8a-4227-94b1-d152e7c33464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 75,
                "offset": 1,
                "shift": 36,
                "w": 31,
                "x": 328,
                "y": 156
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5fe55637-4567-432b-ae93-945b1ae4fa39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 75,
                "offset": 4,
                "shift": 41,
                "w": 38,
                "x": 288,
                "y": 156
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0812d82a-601b-4168-8265-2dd2856089e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 75,
                "offset": 4,
                "shift": 35,
                "w": 30,
                "x": 256,
                "y": 156
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "89806468-587b-4508-a3ef-24a2bf8da35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 75,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 206,
                "y": 156
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cc5dfc8e-e1e6-41e1-9d61-d0b27899d84f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 75,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 505,
                "y": 79
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4f6983dc-9c02-43c7-bd28-744ff3dfa675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 75,
                "offset": 2,
                "shift": 44,
                "w": 40,
                "x": 463,
                "y": 79
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "19d85176-ca18-4693-a598-28cfd3d2e738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 75,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 426,
                "y": 79
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d3beba74-d99e-4752-a604-50549ba252ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 75,
                "offset": 2,
                "shift": 44,
                "w": 40,
                "x": 726,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e56f6818-ced9-4ac4-8a7c-6d6feb3a718f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 75,
                "offset": 4,
                "shift": 41,
                "w": 36,
                "x": 671,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4b32e67b-4f6b-4767-9ff7-5e4ac90724b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 75,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 633,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d3588ef8-573d-49ec-9cea-133b2e6710f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 75,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 593,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d6680f24-770a-4245-bd89-550d01333c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 75,
                "offset": 3,
                "shift": 42,
                "w": 36,
                "x": 555,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "66718631-0ce0-48af-b1bb-abcd9173ebb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 75,
                "offset": 0,
                "shift": 42,
                "w": 42,
                "x": 511,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fed23995-363d-40f0-82c8-2b5fc4193bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 75,
                "offset": 1,
                "shift": 56,
                "w": 54,
                "x": 455,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9100e2ca-eb6a-45ba-b781-750fcfbb0bae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 75,
                "offset": 0,
                "shift": 41,
                "w": 40,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "22d209a3-0894-4535-aa6f-8728fac42b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 75,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b30e722d-1bc6-4f08-b1e3-bf646abe302e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 75,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "df74d8c3-98e0-4d55-96e7-4af612d5a2cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 75,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 709,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "88af26f4-db12-45e8-9b82-d176b68bde5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 75,
                "offset": 0,
                "shift": 27,
                "w": 29,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ba8043f3-e063-439e-921a-2898f5f537df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 75,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c71d1ab5-f010-4ca6-afee-e1b1344787b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 75,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9f895981-aad7-4dcc-93dc-a50372f2e378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 75,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3746b819-d7d7-4147-a26e-247643098af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 75,
                "offset": 1,
                "shift": 21,
                "w": 17,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "66960f81-e177-462d-b246-8d5fae7d80ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 75,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0eacb8e8-dbcf-409d-8f30-2a0b1fefffbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "85e3f768-15a6-403d-b30f-8f02f1c80b45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 75,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "484a983d-4927-4eee-ad61-e38247dccc64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a4321175-d0c7-440e-b3ca-8f0fa7b50bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 75,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e60b4856-7b6c-4fb8-ba9d-0379166b4238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 75,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7e2592fa-f3f6-447e-aba7-b44a5aaa4895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 768,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "72ec8710-ccbb-4f01-a241-ca3bcee40536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 68,
                "y": 79
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3de3abc1-db48-4cd7-9ba1-79b95a1d7e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 75,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 802,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ffeb94f0-4a62-4b59-8979-92a7bc17f175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 75,
                "offset": -3,
                "shift": 17,
                "w": 17,
                "x": 375,
                "y": 79
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "20fa5ab5-ad3c-40ed-b1c9-a86c46fffbf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 75,
                "offset": 3,
                "shift": 34,
                "w": 32,
                "x": 341,
                "y": 79
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "82ee9aa7-2e32-4d9f-baaa-07e9c8872990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 75,
                "offset": 3,
                "shift": 17,
                "w": 10,
                "x": 329,
                "y": 79
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d34c1b20-36ed-4841-9d58-11b5858d47d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 75,
                "offset": 3,
                "shift": 55,
                "w": 49,
                "x": 278,
                "y": 79
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6accab25-4da2-4359-b8d8-73eb227c1e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 246,
                "y": 79
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ca4201b0-96a4-4ae2-9e89-471e7a22bac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 211,
                "y": 79
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "165a123a-7c6f-4ac7-a00d-a633b743c8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 178,
                "y": 79
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "174f8290-191c-48b5-ad22-b814b3ff13c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 145,
                "y": 79
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "84be958b-eada-4894-9ae0-7345cf3295ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 75,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 123,
                "y": 79
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "350cf543-84af-4116-a430-f36b7bdc02e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 75,
                "offset": 1,
                "shift": 33,
                "w": 30,
                "x": 394,
                "y": 79
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "056dec9d-3c3c-4cba-b9ff-25d438ca4d49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 75,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 100,
                "y": 79
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "da6c803d-d539-4d26-afac-105a7c696d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 36,
                "y": 79
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6fbe1def-1cff-48f5-ad91-2a6a4983422d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 75,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 79
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "70f5557f-52d8-4d60-a097-5f3b752c15bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 75,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 967,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0552a374-38e0-4ff9-be42-da7cc8d8b349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 75,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 932,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9f737513-8ac6-47c7-bec0-89835416eff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 75,
                "offset": 0,
                "shift": 32,
                "w": 33,
                "x": 897,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "67f6469d-e87f-400c-912d-160cd84f7f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 75,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 866,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "59ab554e-116b-4be8-a8b5-a222c6579015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 75,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 844,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "82529c5e-245c-42cd-b7e3-ee45fd950240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 75,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 836,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5fcfaa44-730c-43d7-ad59-6d0c68fa6113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 75,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 815,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8d850c57-6388-49c7-aa65-de9fef77afee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 75,
                "offset": 3,
                "shift": 42,
                "w": 36,
                "x": 918,
                "y": 156
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3c0411b4-cf6d-4c64-a7fd-298b1ac4616f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 75,
                "offset": 10,
                "shift": 39,
                "w": 19,
                "x": 956,
                "y": 156
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 48,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}