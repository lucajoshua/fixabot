{
    "id": "04cef3c2-24be-4bf4-94ea-02d3a6086bee",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "bigFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e3ac0d73-ba02-4e17-beb4-9277bb733cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 135,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1b01fcfe-574e-484c-b347-b262e10a0a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 135,
                "offset": 10,
                "shift": 39,
                "w": 18,
                "x": 880,
                "y": 413
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2876ca7b-c51b-4665-ade8-f3566d265281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 135,
                "offset": 6,
                "shift": 55,
                "w": 44,
                "x": 834,
                "y": 413
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f7ecdd80-a256-4f98-8580-9407ddeb64c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 135,
                "offset": 1,
                "shift": 65,
                "w": 63,
                "x": 769,
                "y": 413
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "914f5fd4-7fe8-4760-8e3a-ff020e39e412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 56,
                "x": 711,
                "y": 413
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ce0cee28-1083-407e-befe-0ffd818d332a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 135,
                "offset": 5,
                "shift": 104,
                "w": 94,
                "x": 615,
                "y": 413
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5cd9e76b-0257-4d7b-90c9-c16388668636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 135,
                "offset": 5,
                "shift": 85,
                "w": 78,
                "x": 535,
                "y": 413
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6e2a21ad-46d1-4989-a7e9-65988785b312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 135,
                "offset": 5,
                "shift": 28,
                "w": 18,
                "x": 515,
                "y": 413
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e3fd5c66-6727-4150-8d33-b299521b4ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 135,
                "offset": 6,
                "shift": 39,
                "w": 30,
                "x": 483,
                "y": 413
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "71589ac0-8be1-4a4d-93d9-16f28f77643c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 135,
                "offset": 3,
                "shift": 39,
                "w": 30,
                "x": 451,
                "y": 413
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5147c49b-f4b4-4222-bf99-8ae29b058822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 135,
                "offset": 1,
                "shift": 46,
                "w": 43,
                "x": 900,
                "y": 413
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ecf8fcdf-e649-4ec4-931b-404d75de07e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 135,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 389,
                "y": 413
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6d740945-e5d2-4ddf-9936-d38f7d730448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 135,
                "offset": 6,
                "shift": 33,
                "w": 19,
                "x": 309,
                "y": 413
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "785c9ddf-77c9-4573-b173-ef300fd5fb4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 135,
                "offset": 6,
                "shift": 39,
                "w": 33,
                "x": 274,
                "y": 413
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1273548b-3415-41b8-ba4e-38272907459d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 135,
                "offset": 8,
                "shift": 33,
                "w": 17,
                "x": 255,
                "y": 413
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0a581d5a-5165-44e6-92e7-b844923dd6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 135,
                "offset": -1,
                "shift": 33,
                "w": 34,
                "x": 219,
                "y": 413
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c00bb9c4-68c6-4eb5-bca3-2b5b6b5b74f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 56,
                "x": 161,
                "y": 413
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9f5ae435-e53e-4528-9a86-1cc0ac550d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 135,
                "offset": 9,
                "shift": 65,
                "w": 38,
                "x": 121,
                "y": 413
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "13c4d115-0562-4583-94fd-52e07b004909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 135,
                "offset": 2,
                "shift": 65,
                "w": 58,
                "x": 61,
                "y": 413
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "263b8a65-7e02-4781-a3fc-b7ad596c62b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 57,
                "x": 2,
                "y": 413
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b8ed6294-8564-4dde-a940-cdb25e5c66f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 135,
                "offset": 2,
                "shift": 65,
                "w": 61,
                "x": 933,
                "y": 276
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "52a4aca5-1194-4cdc-b8b4-2d86278e5e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 135,
                "offset": 5,
                "shift": 65,
                "w": 57,
                "x": 330,
                "y": 413
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6cca0c3d-8c9e-44e7-8de8-0f34d10a6f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 57,
                "x": 945,
                "y": 413
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1992f3f6-2b32-46a1-9e9e-3991dacddc1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 56,
                "x": 2,
                "y": 550
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f370838e-35a2-43db-a745-c2220cdc4570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 56,
                "x": 60,
                "y": 550
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "92cbad1a-c36e-4df2-862b-8458871738e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 135,
                "offset": 3,
                "shift": 65,
                "w": 57,
                "x": 400,
                "y": 687
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b2fbff00-43a9-40fa-a880-36eb65907ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 135,
                "offset": 11,
                "shift": 39,
                "w": 17,
                "x": 381,
                "y": 687
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "107914d4-b9dd-48ad-8a2d-369c2a48bb4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 135,
                "offset": 9,
                "shift": 39,
                "w": 19,
                "x": 360,
                "y": 687
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3a7122b5-37b0-48b9-8315-0fdb2c01aed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 135,
                "offset": 5,
                "shift": 68,
                "w": 58,
                "x": 300,
                "y": 687
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8d9e9b47-faf8-42c1-b021-af9e9709ea73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 135,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 238,
                "y": 687
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "75703ee2-d3cc-4c94-bb29-04500c66d6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 135,
                "offset": 5,
                "shift": 68,
                "w": 58,
                "x": 178,
                "y": 687
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "66d870fd-da15-46e8-aaf0-d39aa4cce3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 135,
                "offset": 6,
                "shift": 71,
                "w": 61,
                "x": 115,
                "y": 687
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "10901c6d-41a7-4708-b1bd-373149adaf26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 135,
                "offset": 3,
                "shift": 114,
                "w": 111,
                "x": 2,
                "y": 687
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "49064ff4-bc16-46e8-a3b3-7ba0f55ced08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 135,
                "offset": 0,
                "shift": 85,
                "w": 85,
                "x": 919,
                "y": 550
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "28036ffa-8449-4be7-b764-0558b53e7ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 71,
                "x": 846,
                "y": 550
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "528d421d-2089-476a-a648-9b098b75ebfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 135,
                "offset": 5,
                "shift": 85,
                "w": 74,
                "x": 770,
                "y": 550
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0e56d85c-d80e-46f1-87e5-7b1c600eb6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 71,
                "x": 697,
                "y": 550
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c4ae1b31-5d83-475d-aa33-e29708e160da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 135,
                "offset": 8,
                "shift": 78,
                "w": 65,
                "x": 630,
                "y": 550
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "51fa82f3-5630-4bee-9edc-0fee7ddd8021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 135,
                "offset": 8,
                "shift": 71,
                "w": 59,
                "x": 569,
                "y": 550
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f49fdd2b-8bb5-42f7-943b-9cdbe348ab7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 135,
                "offset": 5,
                "shift": 91,
                "w": 79,
                "x": 488,
                "y": 550
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "810d1151-1384-4074-bff4-083fa0c58519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 68,
                "x": 418,
                "y": 550
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d05b1084-0e42-4918-bee5-04382649c05d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 135,
                "offset": 8,
                "shift": 33,
                "w": 17,
                "x": 399,
                "y": 550
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "827f54a6-5000-452f-af67-f4ee6a2c902c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 135,
                "offset": 2,
                "shift": 65,
                "w": 54,
                "x": 343,
                "y": 550
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "19ec9bcc-ca77-4214-8b71-08271a01ef7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 77,
                "x": 264,
                "y": 550
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "656a448c-b79c-4b96-bcaf-dcd5468b14e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 135,
                "offset": 8,
                "shift": 71,
                "w": 60,
                "x": 202,
                "y": 550
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9adfcf2d-9410-4ceb-9650-65b2b65a2467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 135,
                "offset": 8,
                "shift": 97,
                "w": 82,
                "x": 118,
                "y": 550
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7008d162-1287-4677-9463-66f14a4346c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 68,
                "x": 863,
                "y": 276
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a1bd4346-6e45-45c7-b34c-84c94130c691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 135,
                "offset": 5,
                "shift": 91,
                "w": 82,
                "x": 779,
                "y": 276
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b2442004-c91c-4962-97d2-78128eb5de41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 135,
                "offset": 8,
                "shift": 78,
                "w": 65,
                "x": 712,
                "y": 276
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d1a2ffac-9503-4106-b292-f194337424d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 135,
                "offset": 5,
                "shift": 91,
                "w": 85,
                "x": 403,
                "y": 139
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c07af581-6d5c-402b-8cbc-6501d2c3fe7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 76,
                "x": 294,
                "y": 139
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5597aa93-5af6-45aa-8369-96f678ef841a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 135,
                "offset": 4,
                "shift": 78,
                "w": 69,
                "x": 223,
                "y": 139
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fa8468e6-8912-4e2c-8e9c-1169cc447a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 135,
                "offset": 2,
                "shift": 71,
                "w": 68,
                "x": 153,
                "y": 139
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ab76e36d-48bf-489b-8496-df96623877bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 135,
                "offset": 8,
                "shift": 85,
                "w": 68,
                "x": 83,
                "y": 139
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d1d3ea9c-711c-4cb5-a2c2-7be01250c696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 135,
                "offset": -1,
                "shift": 78,
                "w": 79,
                "x": 2,
                "y": 139
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2b6cc1f2-bbf0-48b6-94d0-0631f95412f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 135,
                "offset": 0,
                "shift": 110,
                "w": 111,
                "x": 844,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3c24fda4-6b94-45dc-bb0f-76f120214252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 135,
                "offset": 0,
                "shift": 78,
                "w": 78,
                "x": 764,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ce09f46b-ba37-4547-96b0-c531a6954e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 135,
                "offset": -1,
                "shift": 78,
                "w": 80,
                "x": 682,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e53d591f-7300-4c25-9fe7-0e455383a8f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 135,
                "offset": 1,
                "shift": 71,
                "w": 69,
                "x": 611,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7b8433af-daed-497b-88b1-6e6a3d7116e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 135,
                "offset": 8,
                "shift": 39,
                "w": 29,
                "x": 372,
                "y": 139
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e013e828-7829-4046-86bc-622b52150bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 135,
                "offset": -1,
                "shift": 33,
                "w": 34,
                "x": 575,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "49fd61f0-a5b0-4b21-8793-ea5b46bb4f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 135,
                "offset": 2,
                "shift": 39,
                "w": 29,
                "x": 500,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e07fb28f-463a-4aba-9da6-593314a5f844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 135,
                "offset": 6,
                "shift": 68,
                "w": 56,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f826f6f4-790f-45ee-a006-8e5d0941b819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 135,
                "offset": -2,
                "shift": 65,
                "w": 68,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "47f5e4e6-0eb5-43d6-b21f-5a9231430b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 135,
                "offset": 2,
                "shift": 39,
                "w": 27,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d640ba4d-7401-40d5-afb3-a25bb20ef1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 58,
                "x": 283,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a99a5b17-4d8d-423f-83be-be24e91cce8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 135,
                "offset": 7,
                "shift": 71,
                "w": 60,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "eb63b998-f907-4ca3-813e-ab0a6371c2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 135,
                "offset": 4,
                "shift": 65,
                "w": 59,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b4e63577-985a-47fa-9e8e-2b9bd01b699a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 135,
                "offset": 4,
                "shift": 71,
                "w": 61,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "964e222c-4396-4683-9df4-2c73cb5cce95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 135,
                "offset": 3,
                "shift": 65,
                "w": 58,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c4f1522c-d392-4b02-b1b0-a24249ef837f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 135,
                "offset": 1,
                "shift": 39,
                "w": 42,
                "x": 531,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "df0e87c8-bcf9-4bce-b0a8-3bc44426d8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 135,
                "offset": 4,
                "shift": 71,
                "w": 60,
                "x": 490,
                "y": 139
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0d20b1b3-06c4-4718-a9aa-048eac52c429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 135,
                "offset": 8,
                "shift": 71,
                "w": 56,
                "x": 60,
                "y": 276
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5a65a64b-d726-4cc3-bbe3-4d76f6656418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 135,
                "offset": 8,
                "shift": 33,
                "w": 17,
                "x": 552,
                "y": 139
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "13a29925-bf65-438c-b524-4cc8dc1d5641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 135,
                "offset": -6,
                "shift": 33,
                "w": 31,
                "x": 619,
                "y": 276
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "be76026c-2b5f-45a5-93df-c175cdf51592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 135,
                "offset": 7,
                "shift": 65,
                "w": 57,
                "x": 560,
                "y": 276
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "335e1a39-2f54-469a-8a06-a17d940240b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 135,
                "offset": 8,
                "shift": 33,
                "w": 17,
                "x": 541,
                "y": 276
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8175d86f-b637-4754-be0c-17950c1b67c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 135,
                "offset": 7,
                "shift": 104,
                "w": 90,
                "x": 449,
                "y": 276
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "100aa6c1-e01b-4296-a484-dca778daf1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 135,
                "offset": 8,
                "shift": 71,
                "w": 56,
                "x": 391,
                "y": 276
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4e9ee1c7-bcf9-4276-9ac3-de1160ffc168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 135,
                "offset": 4,
                "shift": 71,
                "w": 64,
                "x": 325,
                "y": 276
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0dc6707a-3a87-4df5-81da-903b3fa61d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 135,
                "offset": 7,
                "shift": 71,
                "w": 61,
                "x": 262,
                "y": 276
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bf4de78c-5015-4733-ba8d-9645de2d4584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 135,
                "offset": 5,
                "shift": 71,
                "w": 60,
                "x": 200,
                "y": 276
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c96002f0-b668-40ca-bd8f-63672f478f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 135,
                "offset": 7,
                "shift": 46,
                "w": 41,
                "x": 157,
                "y": 276
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c7699a8b-6f31-4c04-9690-ea6130117132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 135,
                "offset": 2,
                "shift": 65,
                "w": 58,
                "x": 652,
                "y": 276
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3a61d405-deb1-42f8-ba91-7a312f320c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 135,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 118,
                "y": 276
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ef7d6004-7412-4402-a8ff-725bf904539e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 135,
                "offset": 8,
                "shift": 71,
                "w": 56,
                "x": 2,
                "y": 276
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9222ff10-54c1-4180-b0a1-5fb813cb08d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 135,
                "offset": 0,
                "shift": 65,
                "w": 64,
                "x": 953,
                "y": 139
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6cfb745e-c2ed-4e72-b6f7-7d42a938cf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 135,
                "offset": 0,
                "shift": 91,
                "w": 91,
                "x": 860,
                "y": 139
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c9a6564b-68e8-4117-9db7-b58b8606d46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 135,
                "offset": 0,
                "shift": 65,
                "w": 64,
                "x": 794,
                "y": 139
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "372f1086-dc8c-4e2e-98d9-f863f68db8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 135,
                "offset": 0,
                "shift": 65,
                "w": 64,
                "x": 728,
                "y": 139
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4118919a-7aaa-4117-95be-3dfee49824fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 135,
                "offset": 1,
                "shift": 59,
                "w": 56,
                "x": 670,
                "y": 139
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "116fb6f8-6495-4dc7-9a89-a8eabf6e6917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 135,
                "offset": 3,
                "shift": 46,
                "w": 40,
                "x": 628,
                "y": 139
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4bcae5f6-21f1-40a7-b183-e52b87e26800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 135,
                "offset": 10,
                "shift": 33,
                "w": 13,
                "x": 613,
                "y": 139
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e03d6979-0f52-4d81-80ed-61ebc2b43b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 135,
                "offset": 2,
                "shift": 46,
                "w": 40,
                "x": 571,
                "y": 139
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a6052257-36cb-48b0-99d8-b876fc7f11a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 135,
                "offset": 3,
                "shift": 68,
                "w": 62,
                "x": 459,
                "y": 687
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "26176918-a7d1-47a0-b8b3-908dd82dad0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 135,
                "offset": 18,
                "shift": 70,
                "w": 34,
                "x": 523,
                "y": 687
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "00e8c7c5-c4bf-4e2d-84a6-454b6f1066b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "c9444a4c-366f-4406-9830-533ecf4c2539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 89
        },
        {
            "id": "e95fa923-dcdf-417d-96b0-c7e955816d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 902
        },
        {
            "id": "4aa1891a-139b-49f1-b85a-240dcf0b1f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 913
        },
        {
            "id": "d42c4278-8e59-409a-8d08-1bd6598ff5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 916
        },
        {
            "id": "e0fcc273-f3a6-474c-932e-808135a03c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 923
        },
        {
            "id": "02ee3ac1-772b-46c6-aabd-4ed771d4d92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 933
        },
        {
            "id": "60d6a27a-1d47-4547-87a3-d20643a062f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 939
        },
        {
            "id": "35869f47-6fd1-45c1-9d70-7f6d23e21106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 49
        },
        {
            "id": "4eb29db5-7811-48d5-86b4-dc9f1fc90c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "c4d2c5e8-d61d-4115-a05f-e0455860d05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 84
        },
        {
            "id": "efccce17-0476-48ff-adcb-9b3485e36341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 86
        },
        {
            "id": "e993d81f-2c12-46ec-8263-0675b5107ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 87
        },
        {
            "id": "05c71be1-9691-4fa1-a41e-4bfeb10faab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 89
        },
        {
            "id": "9270a322-c029-4a6d-9518-7d81bb2c0683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 118
        },
        {
            "id": "55b0e585-861c-47ea-ab71-160b027f8b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "29507e0c-8c0b-4aac-9c17-8d1d1af1533a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 121
        },
        {
            "id": "34b5e15f-53dc-4749-8548-6cccf5064c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 160
        },
        {
            "id": "9c9e4472-9e17-4b40-998a-1d16cdbdfa32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 8217
        },
        {
            "id": "18648da7-f741-4e16-a460-1a0bc4b972eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 70,
            "second": 44
        },
        {
            "id": "1b0a3859-96bc-4fc5-8385-19ce1c5c7a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 70,
            "second": 46
        },
        {
            "id": "53c4fc9f-1660-499b-bede-2bd175b4a8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 65
        },
        {
            "id": "2f9a7ae7-b9dd-4ed7-9099-a0c1269d8e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "e6bb4070-7d2b-457b-9a4a-ed1adfc93f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 76,
            "second": 84
        },
        {
            "id": "d80dc828-ef62-484d-bb9c-42fee03a25bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 76,
            "second": 86
        },
        {
            "id": "6baec8c8-83ce-4666-90de-82703ab420d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 87
        },
        {
            "id": "22fc8050-b226-481b-a7af-7f14b181d04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 89
        },
        {
            "id": "e48c22f7-0650-4ae9-a08b-65f9f55da8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 121
        },
        {
            "id": "fbdc154b-74e4-4fbd-962a-cd06b3c923b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "b4703b0e-4ee9-48d0-8ee7-f5662d52bed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8217
        },
        {
            "id": "99d66eaf-9bf6-4594-8b3f-d1d87e70f1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 32
        },
        {
            "id": "f1696441-0d3e-4ec5-b301-e49e737c0761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 80,
            "second": 44
        },
        {
            "id": "77627ea9-b07a-4ac2-a872-aad7f3912ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 80,
            "second": 46
        },
        {
            "id": "9977af16-b4c7-4e37-bec9-c87e46279d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 65
        },
        {
            "id": "36e7cd29-3c14-4dc9-ac95-cf5c183b5d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 160
        },
        {
            "id": "4e612d28-df15-4cae-89f7-4350478aed37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "ebe5daa8-6151-429c-a479-7dbbcd8ce78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "0264eb55-18e8-4a43-99f9-0175285d893e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "40289e83-0193-4d0b-bfca-bcb92e3cda0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 44
        },
        {
            "id": "d320e913-b265-4fba-a7ad-7b87c0dfdbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 45
        },
        {
            "id": "6aa9abed-eed9-4f89-8104-ec82a496d3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 46
        },
        {
            "id": "150039f9-4c36-4c9b-8977-bdf041dae6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 58
        },
        {
            "id": "d3711079-c262-4ac5-9eda-dc982c430138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 59
        },
        {
            "id": "8c0b9c6c-926a-4006-97e2-6f47580c1d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 65
        },
        {
            "id": "9c09579f-3eda-42e5-a9c8-011b722c27b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "aa279e08-1548-4796-a0b8-e8b6ae9f4cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 97
        },
        {
            "id": "0c213ea4-a382-46e6-855a-4d79b43a75e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 99
        },
        {
            "id": "85188cf3-6605-40b6-b2ff-67f130a95313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 101
        },
        {
            "id": "f2a0d200-0ac9-403b-9e76-cbde7b40b41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "08d77ccc-b1a6-43aa-9ea0-eb2450d97adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 111
        },
        {
            "id": "765ec97d-b597-42b5-af3b-17bf59d12365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 114
        },
        {
            "id": "65165b42-1cfa-4447-80ef-1e3f658ae89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 115
        },
        {
            "id": "9149b7a7-1cfb-43ee-ac28-b709666acaad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 117
        },
        {
            "id": "7a256da8-9697-4605-a4fa-4245ac4d9ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 119
        },
        {
            "id": "56b01e46-ee7a-4f77-9b59-4a90fc214903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 121
        },
        {
            "id": "6d1fd1c3-7206-4bd9-9e4d-6b612cece880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 173
        },
        {
            "id": "01cb948f-532e-4e42-8f1d-c76ff981b192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 894
        },
        {
            "id": "63d95c74-a429-423f-bf58-d03974cb73a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 44
        },
        {
            "id": "83965933-6f00-4c6a-ad61-1e6e958e1c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 45
        },
        {
            "id": "97adada8-c88b-4544-927f-ead91d4bbbee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 46
        },
        {
            "id": "f85611a3-cee1-4cdd-9455-8f299d094616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 58
        },
        {
            "id": "8ec05191-67ab-41f4-b644-31ee51b8da41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 59
        },
        {
            "id": "e8d36a64-cc58-4916-97e2-c77994e442f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 65
        },
        {
            "id": "f9dd43b9-de52-495b-adb4-801eda7a7f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 97
        },
        {
            "id": "6a4c5e97-0d70-4b32-9d9a-73868f1913ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 101
        },
        {
            "id": "e6f673e7-481d-4ce0-ba00-ac51dc7e4056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 105
        },
        {
            "id": "07aa1728-948e-44d6-a163-feee3bf25009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 111
        },
        {
            "id": "1c483391-0c06-4d7c-8338-a735d65826be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 114
        },
        {
            "id": "71af116c-6236-423b-97bd-7894e6c02ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 117
        },
        {
            "id": "f93ac7b2-6b50-4eaa-8b69-81f7e0af6e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 121
        },
        {
            "id": "30c10b55-a685-47bf-9bc0-b951e5972bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 173
        },
        {
            "id": "117ccb15-b09b-46aa-a053-a910a0e69c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 894
        },
        {
            "id": "ebc5ec4e-d4e2-4bc6-a7b5-c83f9fd75c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 44
        },
        {
            "id": "e037525c-dcd6-40fb-8a35-f3fdf08201f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "3f855fc3-3f21-40e3-adb3-b35ee9bb69f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 46
        },
        {
            "id": "cb3ac335-2641-4234-a21f-150e78783673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "ad64ef4c-98ad-447a-a089-d572a91794b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "d295c5fb-2081-4705-baa9-90401cf6d981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 65
        },
        {
            "id": "404a02ce-30cb-438a-8a85-0fbfba76b0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 97
        },
        {
            "id": "2355c6ce-8251-480e-95df-55681102d4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "7bdcc748-b1a4-45eb-80b0-0f3d5aecfe5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "53a2a3c9-d834-4b39-b2a5-7fa0aee7d822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "190bdd92-8b49-4241-80f0-663447ac4a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 114
        },
        {
            "id": "907754c2-c18c-4306-8b4e-5b21e2f087e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 117
        },
        {
            "id": "4ad396f0-add5-4d91-9595-54c3a5a373e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 121
        },
        {
            "id": "b1612f23-f8a1-419b-a487-1edfae8c12a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "795fb766-0f32-4d8d-9066-2a18c52e46bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 894
        },
        {
            "id": "9919c1eb-ffd9-461f-8e9d-0dea622fbea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 32
        },
        {
            "id": "8590cea7-1141-431e-9819-ab29322d1511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 44
        },
        {
            "id": "d1f44448-1482-4954-b574-2bb42b26148c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "4695d9a7-52dc-47ef-bbed-7ed39786546d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 46
        },
        {
            "id": "88f9dee0-9c33-4a93-901b-d40f3f7a8b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 58
        },
        {
            "id": "eb07e381-ea17-41ff-a05b-7c34de7e28d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 59
        },
        {
            "id": "3e98f91e-9fa0-4e6e-9947-fb28c7a6986d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 65
        },
        {
            "id": "701135b0-f446-4bf9-927e-269285e7aa5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 97
        },
        {
            "id": "c1189bc1-8230-4978-97f2-a8d94f39a138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "9518ac86-094e-44f2-a251-080dd87de50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 105
        },
        {
            "id": "3de4fe2d-b035-4927-aadc-4895db40ddaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 111
        },
        {
            "id": "567f77a9-8dcb-43aa-adb2-b7a40b3f9af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 112
        },
        {
            "id": "e4b298c8-7356-4367-8452-599ad1891d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 113
        },
        {
            "id": "0d0b565e-b460-4ffa-8b07-fcf9f138e725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 117
        },
        {
            "id": "415ac282-abb9-477f-ae1e-8913da450c41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 118
        },
        {
            "id": "213d225f-9197-43c0-a766-22e42d161adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 160
        },
        {
            "id": "8b02b39b-6132-420c-b6a0-5b6e771636c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "893a1312-9ea4-4e18-b68f-b8b934dff28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 894
        },
        {
            "id": "2d2beceb-e224-482d-9ed4-9e7c040e681f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "7e55edb7-c171-483c-a787-689c90650b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 114,
            "second": 44
        },
        {
            "id": "31b6d7ad-f237-45e8-a51a-3bed5e7c641b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 114,
            "second": 46
        },
        {
            "id": "0f610c81-cb36-4b29-b560-03edfb621e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 114,
            "second": 8217
        },
        {
            "id": "080f6181-1a46-4eb6-acaa-a397687b6ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 44
        },
        {
            "id": "d3645b5c-17c2-47e3-873f-c892aa3e614c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 46
        },
        {
            "id": "1a4f5563-e2ae-4323-ad5a-a0d786e1206e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "13b916e7-5ec4-4280-bbc1-abbf1c8333ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "f37d120c-dc32-4ba2-bc22-6e438550d96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 121,
            "second": 44
        },
        {
            "id": "bcc8ce78-d261-40d1-a4dc-9c8c24ac77e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 88,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}