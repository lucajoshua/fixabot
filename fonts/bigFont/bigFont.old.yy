{
    "id": "04cef3c2-24be-4bf4-94ea-02d3a6086bee",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "bigFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "89ae1dc7-c957-4bc0-9868-ffd0a6d2631c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 89,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "17bd7a51-7d59-4cad-88df-fd6168474d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 89,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 396,
                "y": 184
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7d580caa-0125-4f4a-94ac-72ede4693927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 89,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 373,
                "y": 184
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "893799b3-9c51-4deb-9835-fd8b6776d475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 89,
                "offset": 0,
                "shift": 43,
                "w": 42,
                "x": 329,
                "y": 184
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9400d434-0fd0-437b-bc71-56fe9d8d4736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 289,
                "y": 184
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "02515963-427e-4b38-8b47-0e53e25b6592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 89,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 227,
                "y": 184
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "607ddcef-f37b-4c12-8944-5ef85e9c815f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 89,
                "offset": 3,
                "shift": 51,
                "w": 47,
                "x": 178,
                "y": 184
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "74f035d6-5f3e-4dd3-95dc-65b26eec9339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 89,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 167,
                "y": 184
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0380aaac-505f-488c-86a2-de1b3bf5d5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 89,
                "offset": 4,
                "shift": 26,
                "w": 19,
                "x": 146,
                "y": 184
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c9b9fc89-ea89-4719-99e6-4efedab80e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 89,
                "offset": 4,
                "shift": 26,
                "w": 19,
                "x": 125,
                "y": 184
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "89e4ec89-2b6f-4b43-88aa-0f5027020729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 89,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 407,
                "y": 184
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7e4fd7e8-e022-4aba-9245-e95600e61893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 89,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 86,
                "y": 184
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c3d8ff68-7417-4337-a8c8-1067847440a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 89,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 36,
                "y": 184
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fac44429-a300-45fd-a589-71511b2066b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 89,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 12,
                "y": 184
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "15dd6b87-01d1-4ad3-9abf-47aa0ff34846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 89,
                "offset": 7,
                "shift": 21,
                "w": 8,
                "x": 2,
                "y": 184
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3a39c0ed-09ff-4996-ba5e-0e6cc7c85776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 89,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 995,
                "y": 93
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "72fb2b3c-cb96-4c59-b448-cf5da79bce03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 956,
                "y": 93
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e82e0517-24e1-4d91-980c-a802ef8aa6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 89,
                "offset": 8,
                "shift": 43,
                "w": 21,
                "x": 933,
                "y": 93
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "958397c6-ad2e-4555-9aaf-dde22dd16fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 37,
                "x": 894,
                "y": 93
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b380d82c-22aa-4cb9-8f5d-ee92994eb11c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 855,
                "y": 93
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "86db13a5-484d-4e92-bc08-78a14a9db13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 89,
                "offset": 0,
                "shift": 43,
                "w": 40,
                "x": 813,
                "y": 93
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "13daeafc-0164-405e-9700-6f62783e9f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 47,
                "y": 184
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aaa3e35a-3020-402c-8fc8-6ecaa207305c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 435,
                "y": 184
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9b669cce-d480-4000-882e-d712a33b483c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 475,
                "y": 184
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7c238356-cff9-4b17-94c7-a4366cab7cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 514,
                "y": 184
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d256bda5-9339-49fa-8112-31cd3f1c591a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 405,
                "y": 275
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0248e525-033c-42b3-b9f6-afcc11f9eb9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 89,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 394,
                "y": 275
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c680f7cf-e117-4530-b055-959f35797187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 89,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 383,
                "y": 275
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a9f69c8e-6e08-413f-a3ae-67b7cbe6648c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 89,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 344,
                "y": 275
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ed0b2486-4668-4bc0-8790-238a5bc267c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 89,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 305,
                "y": 275
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a837eb47-8544-4545-87f2-cf122f77d0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 89,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 266,
                "y": 275
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7ef6c93b-62ae-41a7-ab74-17ae5d12a3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 89,
                "offset": 3,
                "shift": 43,
                "w": 36,
                "x": 228,
                "y": 275
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9c79a2ec-1414-4159-a18b-a0a8856cce72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 89,
                "offset": 4,
                "shift": 78,
                "w": 72,
                "x": 154,
                "y": 275
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "251a81e4-cb97-4a72-b920-017e6e0be49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 89,
                "offset": -1,
                "shift": 51,
                "w": 53,
                "x": 99,
                "y": 275
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7b8e7ba6-92ed-4aa3-b772-66e1bf7513a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 89,
                "offset": 5,
                "shift": 51,
                "w": 43,
                "x": 54,
                "y": 275
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "06ef81b7-43d7-40de-a24f-ac7bb027f20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 89,
                "offset": 3,
                "shift": 56,
                "w": 50,
                "x": 2,
                "y": 275
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b1e0be1a-76bd-42c0-a5d9-aa763f67869a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 89,
                "offset": 5,
                "shift": 56,
                "w": 47,
                "x": 923,
                "y": 184
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d4edb992-9fa7-4da7-a4ce-d38c9fd165c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 89,
                "offset": 6,
                "shift": 51,
                "w": 42,
                "x": 879,
                "y": 184
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1732d948-7ed0-4622-88c7-83daf088f5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 89,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 839,
                "y": 184
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "98c34490-8403-4c21-9566-e86545ef8185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 89,
                "offset": 4,
                "shift": 60,
                "w": 52,
                "x": 785,
                "y": 184
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "80e5580b-64eb-472a-96f4-902ab4540aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 89,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 739,
                "y": 184
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3b67fad2-614e-46bd-99bc-300491ee3c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 89,
                "offset": 7,
                "shift": 21,
                "w": 8,
                "x": 729,
                "y": 184
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "57b1cd25-870b-47dd-b518-3411f45b3364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 89,
                "offset": 2,
                "shift": 39,
                "w": 31,
                "x": 696,
                "y": 184
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "53086f9d-4002-43f4-933c-e966affe13b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 89,
                "offset": 5,
                "shift": 51,
                "w": 47,
                "x": 647,
                "y": 184
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2fa43074-d8f1-4232-8646-1554de6058d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 89,
                "offset": 5,
                "shift": 43,
                "w": 36,
                "x": 609,
                "y": 184
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5a6e7e36-7cfd-476d-8335-933790363d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 89,
                "offset": 5,
                "shift": 64,
                "w": 54,
                "x": 553,
                "y": 184
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d9d3130b-3340-477c-a794-669f765bb825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 89,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 766,
                "y": 93
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3b0fce72-b44f-4597-ad74-6aa47ff2dbad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 89,
                "offset": 3,
                "shift": 60,
                "w": 54,
                "x": 710,
                "y": 93
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "85f678db-5af9-447b-8d53-7e401c9ae14e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 89,
                "offset": 5,
                "shift": 51,
                "w": 44,
                "x": 664,
                "y": 93
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4299923f-e486-410d-afe8-7aec8ec4fc07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 89,
                "offset": 3,
                "shift": 60,
                "w": 55,
                "x": 875,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e44ab0a4-394a-4e83-995a-8f1804f6564c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 89,
                "offset": 6,
                "shift": 56,
                "w": 49,
                "x": 806,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "751c3f8f-76ce-4cf5-8c16-0533e838ade0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 89,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 759,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8a9e187f-91c9-43ca-b4d2-efd08f8e559b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 89,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 712,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4fa09b75-2819-41ac-a4ff-3634f97aa30b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 89,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 666,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3b121755-decc-48b5-bf62-de587ab6762f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 89,
                "offset": 0,
                "shift": 51,
                "w": 51,
                "x": 613,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "05c63b8f-23de-4bc9-9a81-95ee7b1e2ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 89,
                "offset": 0,
                "shift": 73,
                "w": 72,
                "x": 539,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b051d370-78b1-4093-b604-9b303730bd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 89,
                "offset": 0,
                "shift": 51,
                "w": 51,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "96413f80-2b87-4637-a019-d51a316db2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 89,
                "offset": 0,
                "shift": 51,
                "w": 51,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "42b13df3-e967-43ed-bc52-cbab8c288eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 89,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "77070cd7-67e7-4c67-91ad-09781f6175bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 89,
                "offset": 5,
                "shift": 21,
                "w": 16,
                "x": 857,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1ab9adf0-1b51-49e0-9a19-e0b1b157bec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 89,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0c1d0603-093f-4de8-a301-85b0178082a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 89,
                "offset": 1,
                "shift": 21,
                "w": 16,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "857db69c-db87-4f89-af47-846ba3e67758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 89,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3b937f3c-4d23-4612-9de3-fa215a26dc73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 89,
                "offset": -2,
                "shift": 43,
                "w": 46,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "70e166b3-95f1-494a-a363-b67640a9a31a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 89,
                "offset": 3,
                "shift": 26,
                "w": 15,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "08ef6265-bada-4f6e-add5-f22210a60aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f58496e8-8136-41f4-9b18-c57b494c78a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 89,
                "offset": 5,
                "shift": 43,
                "w": 35,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "54e7bf4c-8d81-4610-ac27-a89673ea5c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 89,
                "offset": 3,
                "shift": 39,
                "w": 35,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cc9d7a5b-c849-4c01-8737-757335d8b390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 36,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "dd0b1967-bc8f-4dcb-ac9f-ebf04ea0cee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cfafaf07-41db-488b-aa56-a0b8fdc17162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 89,
                "offset": 0,
                "shift": 21,
                "w": 25,
                "x": 335,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8f3a86a3-ad28-4260-b538-b4185ba9dc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 36,
                "x": 932,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a35905d6-b589-4829-bc04-bd3879af2409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 89,
                "offset": 5,
                "shift": 43,
                "w": 33,
                "x": 276,
                "y": 93
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0b6691ba-e50c-443c-bf3f-f3ba6f6c9011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 89,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 970,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "75616012-e1a6-4b8c-8426-fd53aec0338e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 89,
                "offset": -4,
                "shift": 17,
                "w": 16,
                "x": 610,
                "y": 93
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "91604dc5-3649-47bd-8001-c0032dd474f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 89,
                "offset": 5,
                "shift": 39,
                "w": 34,
                "x": 574,
                "y": 93
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f83c8ef3-76ac-481c-9b79-59e74b46a22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 89,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 564,
                "y": 93
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bb835048-68c2-4b38-9d97-3f6e128a7ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 89,
                "offset": 5,
                "shift": 64,
                "w": 55,
                "x": 507,
                "y": 93
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "faef7232-043c-4cd6-8095-1ebdf229dcae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 89,
                "offset": 5,
                "shift": 43,
                "w": 33,
                "x": 472,
                "y": 93
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6e66cb65-92a3-4e41-b804-90d1d01ac9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 432,
                "y": 93
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f2e9f2f3-3ff0-4434-b8f4-8ac77a6bda6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 89,
                "offset": 5,
                "shift": 43,
                "w": 35,
                "x": 395,
                "y": 93
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7c29bc44-8b51-40cf-8b97-3bcd4908eeb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 89,
                "offset": 2,
                "shift": 43,
                "w": 36,
                "x": 357,
                "y": 93
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8e1f2d8f-4839-4232-9367-51992b2903e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 89,
                "offset": 5,
                "shift": 26,
                "w": 22,
                "x": 333,
                "y": 93
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8374c313-6006-4444-89e5-e9ae7945d8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 89,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 628,
                "y": 93
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bdb47fbe-0fe1-4a8c-8a7f-3b8456ce4ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 89,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 311,
                "y": 93
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4d6d438c-500f-413b-a33e-300376d7434a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 89,
                "offset": 4,
                "shift": 43,
                "w": 34,
                "x": 240,
                "y": 93
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b4061d7c-e025-4b63-a6ec-574e839c2c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 89,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 200,
                "y": 93
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "33758565-4ad4-49a2-8a07-a7c508c05a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 89,
                "offset": 0,
                "shift": 56,
                "w": 55,
                "x": 143,
                "y": 93
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "39e585c1-df2b-41f0-aed6-7582919f28f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 89,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 103,
                "y": 93
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8b39a4d0-28b2-4e68-b9d7-c2e408ccc1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 89,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 64,
                "y": 93
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "491d1eda-a932-46d9-b8f6-14f9defabd37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 89,
                "offset": 1,
                "shift": 39,
                "w": 36,
                "x": 26,
                "y": 93
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "45e9b3b6-0bc6-48b1-a14e-3fa94e6d43c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 89,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 2,
                "y": 93
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dd39a2ba-56fd-4003-b0c2-5546ebcf65f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 89,
                "offset": 7,
                "shift": 20,
                "w": 6,
                "x": 1004,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "38a05814-12ea-4440-a73d-1b8028d683e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 89,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 979,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f7a432fa-7011-49d4-b90f-bff29f30676b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 89,
                "offset": 3,
                "shift": 45,
                "w": 39,
                "x": 444,
                "y": 275
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4086a576-f8dc-42d5-93f7-3f8505c575e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 89,
                "offset": 12,
                "shift": 46,
                "w": 23,
                "x": 485,
                "y": 275
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0c747721-ce7a-462b-b9ed-0e65d9ccac21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "fdbe4914-75bd-46de-9b35-e6a40d1afaf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "60d5fa4f-70b7-46b3-a94c-a1aa905d20d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "fabfedc0-0454-4fe2-8891-b767b3e0e0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 902
        },
        {
            "id": "68aee65c-fff5-4d5a-a853-4bd1881c5d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 913
        },
        {
            "id": "df0606c1-4830-4673-8da1-8aa12b0ba575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 916
        },
        {
            "id": "cab30f56-76d1-41ff-9bac-a0c35c6f72ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 923
        },
        {
            "id": "0a3efd59-540d-4a1e-9892-9391f20557b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "7b813404-6148-4d1c-9b7b-4bbcb5fdd1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "3e8ba4e5-b51d-4ffe-84fa-3778f584d14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "8ce7caa4-f068-4b01-8665-56e880febfa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 49
        },
        {
            "id": "c648ca88-0add-4dde-8948-de849cab7b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "a0dc9d22-449b-4912-8393-57633326b176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 84
        },
        {
            "id": "3e42a76e-e0a9-4e9c-bfc8-2073973d296e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 86
        },
        {
            "id": "b910dd59-107d-4a09-85c8-64f997717ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "9c13fd98-c3c4-43aa-ac9e-aba76801966e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 89
        },
        {
            "id": "cbc0241e-1c94-456d-aff8-5dfa25d49735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "7fdd8801-e57c-41a9-ba4e-a29e27739151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "31119e2d-dfb9-4f1f-b813-c6426d046329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "fab50284-c52b-4ff5-afd1-bf050b28479e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 160
        },
        {
            "id": "fb23234f-27e6-4b03-8501-c3256dfb8738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 8217
        },
        {
            "id": "30224ec7-b7ce-4aa5-8969-cb2987d2acc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 44
        },
        {
            "id": "4dbbb9c8-0657-40f6-a6ec-3cb9176fddfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 46
        },
        {
            "id": "c0fc28aa-3431-4681-bc77-ccec78a352b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "2d78b7c9-84b6-49b4-ab0a-62aa69b25ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 32
        },
        {
            "id": "047d3751-0d65-409c-92ce-d3663937d8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 84
        },
        {
            "id": "c3bf71e7-955e-438c-a2bd-a54c09d97dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 86
        },
        {
            "id": "b73abeeb-6b21-4497-a152-b1a14c05fd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 87
        },
        {
            "id": "d821cb8f-831c-4448-92ce-90e4c25d4425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "32e9bd2b-0cd5-4831-877e-1f1773d8a265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 121
        },
        {
            "id": "127259c6-13ad-460f-8b9e-f8148c3da046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 160
        },
        {
            "id": "b4584312-944e-4189-8ec5-b36434c76540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "f009ecee-6aa6-40a0-8410-9feab6f92f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "16d80723-f06b-4f93-a945-3a839fe66be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 80,
            "second": 44
        },
        {
            "id": "c45a5160-e715-44b1-849a-6c1f3b488b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 80,
            "second": 46
        },
        {
            "id": "1130d847-abd4-4575-ba01-5870c395899a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 65
        },
        {
            "id": "85c80ec3-69a2-4cf4-a7d7-d9e7983d99de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "4e5724e7-4220-48cf-8fd6-bbf59362aa95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "c99cc549-2897-497b-97a1-5f74dd4454e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "2576e1f4-f3de-48e6-a0f4-69a48eded2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "51d4a366-af9e-4eda-9d3d-78421030caf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "0fdbad6f-007b-45c6-b955-73f6c9672761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "90e95429-c8a2-4fe1-a6a4-518cc19bea0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 44
        },
        {
            "id": "79354df8-3691-42d5-8ee6-ac23da9e0ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "4483504c-1fea-4fb5-9d37-cc50c5d6f75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 46
        },
        {
            "id": "4b61af42-2272-4cb0-8853-0674e2c298a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 58
        },
        {
            "id": "f7bb4e0b-29dc-4450-981a-1dd75a8a5011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 59
        },
        {
            "id": "f500e803-eb81-49a4-9cf4-edc4033e48bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 65
        },
        {
            "id": "e9246688-bb78-4cb7-b03e-5905e08dc94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "0e76091e-652b-4077-90c7-c01ce2552702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 97
        },
        {
            "id": "71799e2e-6dce-40eb-ac84-5768a1559df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 99
        },
        {
            "id": "22021290-532f-4f15-bedc-b0aff98df6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 101
        },
        {
            "id": "1290de64-d051-46ee-885b-1426c9f05297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 105
        },
        {
            "id": "7bd82077-cb0f-478d-9478-eac0b8a8e139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 111
        },
        {
            "id": "2e504e0f-83ea-4894-b257-e09a96248cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "b39d942b-2708-4931-8ced-b7ffe324d7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 115
        },
        {
            "id": "c152f99f-34d1-486b-8bd4-4ac8138c6a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "ace245dc-ff22-4930-89e8-af90595a2adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "5791ddc2-0e07-4f51-ac43-0e310e72ced9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "8c0fb790-c52e-4114-a131-6113cce25847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "1dd839de-911c-48da-853b-6b8eeacdeaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 173
        },
        {
            "id": "2a122d46-b08a-4647-a584-a2e195e2e7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 894
        },
        {
            "id": "5229d724-a3da-4ad2-8952-e1f5787dd54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 44
        },
        {
            "id": "ea87678f-ff71-4a87-a33e-9bee45dcda62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "b53a92ec-942d-4643-8302-d3c9bd3569fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 46
        },
        {
            "id": "15ac30ec-7c95-4fa8-8e92-df84ed01bce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 58
        },
        {
            "id": "80f7f38e-b5f6-4171-b40e-496c92354d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 59
        },
        {
            "id": "d3154825-bd3e-47ac-8e1a-d1038a78cbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 65
        },
        {
            "id": "d6c00f87-d12a-4905-ad45-860b75d8a4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 97
        },
        {
            "id": "5a02f2ac-bb92-4eb0-97f8-727baebdaaa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "87193792-ce5f-4587-9eb1-00e98d2599a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "412e970c-e09d-4f1b-a973-3079b0523716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "37253048-8ca2-4e5f-8366-e146b7503c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "6890ec2e-4fb0-43b9-bfb2-90cd8d1790d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 117
        },
        {
            "id": "ad93d1cc-e0a9-4711-9071-5d09810ab8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 121
        },
        {
            "id": "b375bf12-fd1c-4c83-8c8e-6ce6305ed131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "6a51ebb2-a640-4225-9a7b-97e07a8bf401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 894
        },
        {
            "id": "20155e31-f118-4f18-85c1-484f95ccdfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "5cb76f26-feef-49ac-9129-32ca036b305f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "099da296-a72d-4ed8-84ba-25d7fe39466a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "805cab1c-fbaf-4a62-87d4-2fd535028764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "c3a37b69-2a14-4d0e-961b-7e156e67740a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "1a7b38c0-86c4-49fd-94a9-e4cfff82ca3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "d741ba1c-2bf5-43b8-a746-f502dfc71acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "f104758e-5fa0-4b69-a5cd-bd4f88dc080b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "59564d38-5494-41d8-95ff-c05ca4d169ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "61d49d5f-0ef3-4d83-bcdf-737e9e6e7a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "fc6cc6ab-7148-4266-8aed-64cbbf833dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "b08e4543-3bf3-40c5-a185-1f992d61678e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "b6e19858-9a2a-4d8f-a877-c837a802e1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "e5bfd942-9080-4c86-ae27-741917d165ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "f5dc827b-130c-4408-a596-ab4ae02d770e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "f9abd270-d1c1-49d6-8b1d-ebe5278aeead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 44
        },
        {
            "id": "fdd0ec28-8fcd-405a-bb0b-3429d9f51a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 45
        },
        {
            "id": "ba47af1d-a637-4449-990b-d24cd625469c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 46
        },
        {
            "id": "62119a5f-89a1-4f18-b583-0f5b0c8a5f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "a5c02a65-1d3c-477e-b69f-e6c8cf7c2c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 59
        },
        {
            "id": "5583ce05-722d-40b3-b651-64cf57e5bf58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 65
        },
        {
            "id": "e7d5389b-2b73-4d95-bcff-0a7d4d54612d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 97
        },
        {
            "id": "5d91e8d6-27ad-41f6-b8cc-411387d86ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 101
        },
        {
            "id": "dce602bb-ed54-46d5-8e72-f1408322ce84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 105
        },
        {
            "id": "9ba928c6-b2cb-483b-988d-fecbae0ce600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 111
        },
        {
            "id": "06bc81c9-2b2a-4e3d-9281-111f0494a517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 112
        },
        {
            "id": "1560be0c-5c8d-403c-803a-8ca9c7b801fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 113
        },
        {
            "id": "5d026408-b6ee-417e-aeda-afe5aad03ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "8bd9b892-5604-42ad-9f37-1938bf3a5755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "9891a6e9-e164-47e8-a726-ec6d0187fbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "7a78901c-42cb-49dc-972e-9b89a306af89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 173
        },
        {
            "id": "0ace507c-7e54-44ff-b7c4-f64ef84276f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 894
        },
        {
            "id": "10532ced-45b5-495b-84e9-47671b7217ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "1cad0dfd-03fd-4803-a101-a261308cb825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "1901c2be-a7ec-4508-a0a9-426a11cb8124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "1e7f8187-8fd5-4a66-b31e-c867bfa480a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "cefbf24f-8e9b-40d6-bf38-02d68a98aa17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8217
        },
        {
            "id": "417e63a4-5fa7-43f9-aab3-6ae3c2e69e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 118,
            "second": 44
        },
        {
            "id": "d6e4363a-ecc1-432a-8e70-151eb894ed8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 118,
            "second": 46
        },
        {
            "id": "22b03aad-59b3-4ab8-b76c-798c67308683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "051ac817-26d7-4a89-a1e9-ed440b6fb078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "e2b7c869-e3a0-47a4-b405-da30ea6fb8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 44
        },
        {
            "id": "7579bb8c-f2c4-481e-b4a0-35abc43a3537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 58,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}