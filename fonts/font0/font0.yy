{
    "id": "75725033-f744-4af5-bd05-cb0f86dcb095",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lato",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "81df7377-2af3-430b-b06c-058474b75886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5c200fe1-74a8-4e6c-9de9-afbe5bb9fe1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "16808151-eb8d-4c21-aef1-04bb4fd651b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 64,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "beb21a8a-4c5f-4135-93d6-ca29f394dc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0be859cf-5970-44e6-862e-6db2b2b3daaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 86
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4e47fce0-456e-4cdc-a156-812a6f5059c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 19,
                "y": 86
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0e9e2328-5b08-4ecb-baeb-7ef31b4ad994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "175c8959-b70b-40ac-b603-684d244d19cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 248,
                "y": 58
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0c460223-809c-4498-83f0-b1317bd520bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 241,
                "y": 58
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6e82d474-1f4f-4f0c-a1a1-246bfc49a8b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 58
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5635c887-416b-40d9-8840-872a100205ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5583d94f-67af-4798-a508-d15f20783b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 221,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0fe5b0f2-505a-434c-9b16-92a8a7f65687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 204,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "060a4bb0-8764-4986-a9d3-c77bc61b7f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 196,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "577465e8-88a0-4c8a-80db-773833fa68f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 191,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0fa9ade4-07a8-4e93-9da0-e14b6c4fb8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "103a4546-8c72-44a7-bb6f-3193f42831c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 166,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ddc9ea12-0f7d-429c-8ec2-33c2b2174824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 154,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c030d80c-cc2d-4a61-ae84-588231b3d94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 140,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3e5f157c-4e22-403b-823e-c624c4ba1a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2e998184-92be-452c-b9bc-181e69b4c30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 113,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a0dba90b-6767-4242-9350-75dbc6d419a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 209,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0813cefd-9d36-4d8a-a2f2-43a02aa6e967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 87,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c558f9cb-58be-4aaf-889c-3d67aaf7d92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 100,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "97aaef77-2469-4bbc-a6c4-1d559c129cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 113,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "492cdc50-93c3-4a3b-ad92-c22a75fb5fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 138,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "65d8c5c8-53c8-461c-bf3a-6a71a2f6167d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 133,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "972885b9-b3dd-4b7c-8ac7-2e4422d2acdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 127,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "185ef5ce-d07c-4e89-a22e-d1868ed24770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 116,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d4a89ca4-5f7d-4365-a524-b7c708de2e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "671cd5a4-3fed-4299-84a7-398864c8fdf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 93,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b50ed9f5-04f7-44b0-a918-7c2eedd997d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 82,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bed88fe7-5114-4628-9d65-2c6ac57743a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 64,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c23af10f-8fd4-4a37-84aa-2e6508c8a7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 47,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "554b83cb-b585-46b2-a510-62512a217ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 33,
                "y": 114
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eb8add6c-58ec-4aa7-a987-90cdbc39ec4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 18,
                "y": 114
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bab2d59a-795c-406d-8db0-b5a6bf7f262e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "765a4ecb-b731-47c5-addf-ca685154beba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 233,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "faa8e7d2-56f5-4e6d-b19d-8d646c05c6d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 220,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0254ade1-aa5d-46da-9bc4-31dabfe9cc29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 203,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "78b4cdaf-057f-4e11-93d3-723db9e5d56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 187,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9df66524-e01e-43b6-85ab-85c74cc25f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3559c550-c342-4ebc-8102-e35a98efe186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 172,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9eb72e99-f4d1-4404-b822-6c4476ff4e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 157,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c1731572-6ff6-4812-ba44-22c73f9fe58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 145,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f57b825f-df20-4239-89b5-3d0a94ccc390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 126,
                "y": 86
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9e138d5b-88f2-40ab-ada7-2ce0a8fa2d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 97,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c4a828d6-deea-4433-91cb-ead2173a9be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 79,
                "y": 58
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a1506b9a-2063-4df0-8359-8d06289eda79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "40d3379f-3594-4bb2-ae6f-13662fb0cff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 37,
                "y": 30
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4951c44c-7725-4d96-a3cd-c954926669e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 15,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d246b895-df09-489f-beec-c2d7b929f173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0a09b8f1-e224-4735-aff5-479c8e69d3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6662c35d-b9d2-47a8-80ed-052a7f06b101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d78ac7e7-93f6-4f83-aa7c-19c93a5891d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "988eed17-910a-40eb-8884-33bca640e71f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e07d75d6-2a97-47a9-85a6-5ff2227c71d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7199087a-74ea-4db3-8a00-7f67859cc9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ad56e238-e28b-4317-a094-32b054a9e4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b64066c5-bc07-4481-abb7-9859400de750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 30
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9c4a9363-1e5c-4af3-b60b-75158fb67d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "af5f2908-27e0-403b-8785-f18a8ae6cdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "198362c2-1ffc-4768-a1ac-3473ce39cc10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "33355b80-7359-43fc-b6da-ad7f779cda64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "76e6dde9-fefe-40cc-88bd-1630cfd13870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "219dddbe-0d6a-41cd-92e3-826cd6dcf1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "073a7286-58b9-48ee-9f1b-8859a2fbf3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7297b2af-949c-4ffc-988c-872b5f8111b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e752f4d6-0c84-480c-8535-3873844e544a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8fac5cc3-f480-43a7-938d-9b7fef5303ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "99405218-04e7-49a2-97de-e139cab6b8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0592aec6-6ca3-4014-9d96-00c4bff8dc66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 55,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "573b9bfa-5d95-4e50-8667-121e218d677b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 175,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "15118dad-0685-4407-af72-c1a4aa56e108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d11c8e70-f1fe-4c8a-b970-51575f490250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3396984f-ef1b-474f-8d2c-904c268f24f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1098d05d-8e88-4f73-9941-6484d2f38327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 31,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "71ea0db9-3e9e-4bfe-9423-04d8b906d3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 14,
                "y": 58
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "42563257-e948-4651-865d-5f1f29e110e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7a816b9a-7c87-46dd-91ed-94430519f0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 231,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "658c6acd-56cc-4241-9068-21805c4d644d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 219,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "867977af-2a79-49db-9873-a8b9c06ded55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 206,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2296e4fc-4da4-4d2f-b672-68be0ac19e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 197,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e890daec-7dd8-40b2-a78b-0b1c36d4ead3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 55,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9e9838d6-96d6-4b75-aebf-d283f99f3103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 187,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c2d8a659-5ea9-4119-83d8-cfcc3be58e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 163,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0ec1d21b-a570-4cdd-93a6-210e4af771e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 150,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "249f17f3-da43-4a4a-b3b2-0e9fe896deb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 131,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "61f1a0b6-81cb-46cc-8a35-4b9fc81b0c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 118,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a3a653e1-f382-4fbd-9e5a-aa358281f1a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 105,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2f921c83-997a-446d-8c93-30d9aa43ea4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f4f1ec66-d553-43b4-8b31-f7de319007cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "90b40c6d-5d83-4eb5-8af6-db8d927a3a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 81,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "296b670c-5f1e-40f4-97ad-80ad5c90caa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8855bacc-a4ab-44c2-9a20-83aedf4a1862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 114
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3796361a-b273-4ee5-a74c-4a33ffe07990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 26,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 162,
                "y": 114
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}