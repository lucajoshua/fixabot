{
    "id": "75725033-f744-4af5-bd05-cb0f86dcb095",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1fbbed64-bc21-4167-832e-a650f3b85a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e5350b6a-a98a-4246-9836-c67f196b11e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8d2e52dd-9ff4-48f7-9126-70fd131233ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "49536608-cf77-426c-8d83-14f20dbd3ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6e2e23af-f5ea-4ce7-a614-48b416d2df9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6adb03ca-881c-454b-b0d6-e08b548c180d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fe34588a-30ed-4294-baa0-b89d764c7344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3107c93b-eefb-483e-9c0c-a0478d41413b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "415eddfa-5b58-4307-b90c-622b1261fb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "336b4c30-438e-4f97-9310-4cb645701c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6b53c060-733f-45b1-bca4-d50280cee728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ed2981e2-bd34-4c22-a2ff-f7b6d0ea12ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 216,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2ef4843b-c808-49a4-b219-c57e8cc47d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 198,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "071ff874-d626-4a09-98f9-0bdaa8f505c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3c013c30-54d4-4aaa-ac17-421abee93763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "29e1b058-f1b9-4389-b52d-91d3448599fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0f8f5a42-5960-458f-a46b-245896afce8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6766ac20-6660-4503-b904-283c5613d478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e30f1069-46f8-4b1d-a40b-0ddcfa59b5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "45d627a1-30cb-4731-8c6f-a896f03c839d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 128,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c5b07442-888e-46ae-8649-221f68717f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a6fd2f9c-c98a-4ecf-9f74-ff28380a14d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9eb23888-8505-4c0b-b2ff-21589a1db132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 89,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "16966fb3-947a-46ae-bfb6-c4013caf798b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5e08f366-c6c1-47ed-aae3-93d60617b65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1e23f3a1-a6c8-4269-80f4-cc702ba56f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 150,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "381db1b4-2087-4b72-825c-7392f1503568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d59cdb5b-5dd5-4f06-88e3-21c122cfa94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d9e6e9fa-cfa1-44b5-887d-98814b099695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "933b2f27-93d9-465d-bf99-c41e9edc34cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5bf88452-0762-4ab0-9323-423ba4f11fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a76aae1c-3ef6-4997-8b8f-de12f9290ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "19cc0fc0-7f64-4e5e-98d7-0499dc569f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8cd734ed-db44-4565-b5c2-1a0501ad6dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4fddb28b-9494-444d-86f9-88f33dab46d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f28a10ec-7d2a-4607-8691-9c30d953d4c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f5961438-6f60-4814-b4ad-c10ebc6f649e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b276bcb0-6034-4a47-8fe7-2740b236b61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "19f4c641-ae41-485b-922a-c624a1b36048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "24cb1793-bcea-49d2-9a44-29929a1e9691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 202,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "859ed128-02af-4feb-beaf-e2257bd12781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "544febcc-075a-4f00-89b2-a1beabc096ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1fbdb38c-809f-4174-afb5-387f0eaec7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a3805b7c-adf9-4d3c-a784-34c369b41e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dbd133ae-0e4c-415f-9925-1e10937cd24e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c59644d2-2e9b-4946-8376-a165950f3a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9eef43aa-f4bb-4ccc-b36e-62f4b6aeeac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "24f4ad60-574f-4a73-9d8f-247071a4fb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e59f35f6-f6cd-4d0a-bdc5-212d536c8e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5cc06a4c-9f62-4758-8a3a-438ca9327133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0006588e-aa60-47d6-83b2-5dc300c8edac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "19d337e4-8485-4712-b89f-52eda7f9a72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d1349d07-9bad-421e-85d8-c754389d8d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8088e8a6-1378-4334-9173-b2fbd695176b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6e7aad0a-0471-42cc-9fa5-2021e56dccc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ca59b9d2-2721-4669-9455-7528e093ba69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4f53a4ff-dfba-46f4-b169-80867f289f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a8070d32-030e-48a3-8050-73ae4fc3f23a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ee547cce-aa36-48ca-9538-1fcfb5018f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "55fe6643-098d-4ff4-bae2-1ada3eade4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3159f438-0308-435b-a2a6-cadc3a403648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "49d0e85d-7a83-4303-8e71-3065fd1eaf4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "748ebdea-bd95-48fb-9fa5-335bbaaf5c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "75eda820-a9f8-4b8c-ac1a-f3c193ec2d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d9847861-88bb-4cab-98d7-b291f18612e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bd4132c2-0e77-411f-bfc5-3a82eba0bae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f05da475-a761-45c8-91f3-7ee8ee806b11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1c502f0f-28bb-458c-ba09-6668bc7e7f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e6a3500a-cb13-49c7-8a5f-00550f2a2278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d29e6843-62da-4593-b1b8-9b1ce4ffd43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "842f8e91-9af2-4d45-9d87-24f23f6b8290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "49d7b142-110e-4a02-8c9e-6c95f8ffc0c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e71270ed-56cf-4715-ab57-05d1613afb0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9e7d15fe-5833-4320-a5a5-d987097e4915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0c1ea6bf-6802-475d-91bb-52838acd7d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c34872cc-d92e-4f85-999e-bcd661b70770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "adb21e8b-116c-4174-bb52-f3efa6608261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d7be6192-7267-406e-9b3a-8a31be6c086d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8ee52d54-de19-4fc7-ae17-db3924124b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d5b03210-43a9-4962-85fe-ecd46fab908f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a94c55d2-7974-4599-a75e-9980344cf64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "76afc50b-5216-4604-b922-02977b768eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 209,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d61ddbd5-256c-4f99-978e-1a975dae39ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e9271396-682d-4421-92b4-a1fd2c75a919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e67e1d58-6416-4b77-bcee-f41ace686a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c1a25ee9-910b-4da9-a8b7-9fe37b720484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d2eb5c2b-fa5c-4282-b83f-45e8a1f078c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "450024df-b826-41a2-91ad-e8b1a4692dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2c2fe3a9-b4da-4d29-87ac-42ab720419f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bfc4ce12-a018-4677-8c29-d46d6b17b4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f1e5b289-2748-485e-ad6f-037b09d283ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3b4c4c2e-8111-4e0e-aa68-e93a32c2974f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3fdfdbeb-edd3-4500-8f86-99e8b4b41490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "291e2106-c2f8-4e13-abcf-504d9449a43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c079f6f6-0817-4826-8667-bd6491e3bfaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "cfd3aeee-3d3f-4c13-af2e-6860191fae8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 177,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8d2ef489-24f7-4ea0-b97f-9bc2ba52f343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "de0d90c7-3cc0-4fc7-93f7-44897949115f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "1dc4159a-08fd-4c55-89b9-51c2289c6dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "2283fc95-098c-4d9e-ad04-ae74df79681e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "5b1a1d2d-dde1-4718-ae7c-f368b709d1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "61a26f4e-b1d8-42c6-81b6-9e1044a5afc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "641b731c-5dc8-4401-a22a-d48f19ff9f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "df1063e2-3d04-456e-985b-d8e023d9d7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "84871a75-b995-4c41-b660-39c5e1c5c9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "f36dd0af-4d94-49fb-8e5a-0ad223af6067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "40533b9a-665e-45f6-b931-7ee97fe1340f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "2ebb799c-455b-4371-832c-d0287de30b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "702bb137-fca0-4ec4-a763-c21b603c1614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "7a207268-3917-466c-a8bb-a35dac444624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "f9639888-a090-47b1-a54c-1721eaf562b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "9f698df6-eecc-47a9-8726-6b8b6f8bd01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "31242299-42a5-4fd6-97ff-27a88ceafe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "7a440cb4-dd27-41eb-9919-4e60e61bd74c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "9615d653-64aa-4a66-8334-96903274be86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9bac70df-7224-4da8-90d3-c332ca00c777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "2936bf5f-933c-471e-9bd3-131c16547805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "1bfe66bc-918a-41c0-95bb-b9e8b5ec4846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "c746c556-964f-40fb-9298-be831ab5e08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "a3037dbd-29bb-47dc-8f53-bb16ca4c25aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4f90df71-0822-4be7-ab25-c5c47948b0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "2a09234d-1631-47b3-881d-95ffbaf91524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "45729d4a-5ee3-45c2-a79e-956627d0e526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "c8f4c920-ed8d-4468-b3f7-12d8b0baa4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "b030b491-a7df-4ee1-bd8c-956958d43556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "e86eb599-c821-4107-abd3-4e894e9c1ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "9588ad82-24e0-458c-ba46-c9ea7b6f368f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "728f08af-71f1-42b0-9866-a5d325272f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "2acfeda8-8d83-4018-a4b7-e5e1c53a4afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "d4f7d639-708e-4173-8acf-13f46aa71ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "ff06b02f-12f1-4091-a075-4bd4359eda92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "9eda964a-e534-4633-a6a0-2efcc8542bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "2921778f-3151-4eb7-90db-2c44e15a06c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "14c35a2a-f1af-4797-9c29-a3dd2a050188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "bfb014d2-ee35-4a2c-add5-07f202d0484a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "306281d2-cb0f-4da6-b805-de998da69227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "fd1e888f-18b2-44e0-8b74-bbd6276884df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "e269d04b-6b43-4ce0-83cb-cf818ad28045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8fe624f0-6e27-4a59-989d-e3a9429778bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "126d476e-967b-42f6-9169-010807e0c673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "3e67d7e5-c49f-4f0e-bd85-767582ce18c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "467ea929-26d0-4f36-8c35-b2fbe26576dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "28023f10-dfd9-468b-96eb-89becb1969f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "bc121dd2-28c3-469b-9666-d846d6a26f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "d756082c-095d-4dab-81c8-bbe75fb3e33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "146550b5-6d6f-4e09-955b-385b009c37ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "158d7788-6f60-4d6e-ba37-93d7ac5c4bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "2e660d2e-5e22-44d1-9b37-2d2cc1d74ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "1ca14596-e26f-4249-b1d2-c257d8bc9fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "09bb7d2b-1e15-4daf-84db-0845939eeea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "ca173726-72e4-4710-b781-1bd73d4548b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "fa8ab104-6b9e-4613-a4c9-d5bc0d90b91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "b2ee2d36-e273-47fe-aafb-8158ae9a5bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "d583c404-9469-4820-bb9d-a8485acadc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "d334fbeb-d312-4f66-86d7-248fbc71eafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "8307d2ff-e555-419c-957d-de45a5cfbd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "553c681f-0648-4bf3-8806-ff460c438dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "3bde0d21-b219-4b82-a31d-7d78b9803b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "10ca87b8-1b98-45c7-aa52-bf9eeadba3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "0f686365-c2dc-4da0-9a1a-e714e345ec63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "205eb934-9c61-45f5-a2ca-9a16861c6ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "bf9a7644-e212-4c54-bc0b-5a7be9234bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "c2d1b900-02a7-4b45-8c32-68d55e35f8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "3ef2b59b-0ed5-4fa0-8618-b85d813fbdaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "5920f3af-4aa6-4833-a4f0-5566bd4be954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "4e18b8e3-b8ce-4841-872d-b384a8d4e115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "b6af9dd4-7cd3-4a76-bf13-27238ee74607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "a848605c-9056-407c-9c70-aa31dfcf429b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "f3319ce8-7369-4544-ab9d-5b105d64b730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "6a32a830-7828-49c5-8216-8f7b6bbb8861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "0a25410c-5dbd-4afa-85f9-dc50aa12c0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "479dec0d-6f75-4051-9517-9880cd9e7fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "46799c00-c4bc-49ee-973e-b904e1bc9158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "15c7c663-c109-4c96-9be5-d57d6e0c4a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "892aa76e-8e6c-4eb0-8926-807e355712f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "265446be-80e9-4577-92a0-3aeb7121a3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "9be5b328-7184-4786-8d64-b47ca4314e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "ccfb4ee1-7ef5-4c29-8dd2-93d6d72170b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "7e236fee-c97a-4641-9d3e-0ee22aacd0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3bef8fda-d3f9-44b8-bd14-72754805007d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "5cd3559b-9603-4b60-93b3-cecd7b9ad460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "5c2dea80-0332-44d2-a305-6d137939b74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "b1f8a92d-5ed5-47b7-855b-c822c191c383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "ab19e77c-00f8-4f5e-bac3-4722529c9b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}